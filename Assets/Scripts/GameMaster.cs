using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Citadel
{
    //Acts as Dealer/Server
    public class GameMaster : NetworkBehaviour
    {
        public static GameMaster instance;
        public UIManager uiManager;

        [SyncVar]
        //Players/clients will increment this as they connect and initialize
        //public int playerCount = 0;

        [Range(0, 5)]
        public float transitionPauseDur = 3;

        //List to hold reference to all players, order of character selection follows list
        public List<PlayerController> playerList;
        //Used for defining player turn based on character selected
        public PlayerController[] playerCharTurns;
        [SyncVar(hook = "hUpdateTurnIndicator")]
        public int curCharacterTurn;
        //Stores loaded player data to be retrieved by players when they start
        public List<PlayerData> playerListData;

        //Index used with playerList to indicate crown holder
        [SyncVar(hook = "hShowCrownHolder")]
        public int crownHolderI;

        //Decks to hold various card types
        public SyncListInt characterDeck = new SyncListInt();
        public SyncListInt districtDeck = new SyncListInt();
        public SyncListInt characterDiscard = new SyncListInt();

        //Flag to signal game's end
        [SyncVar]
        public bool isLastRound;
        [SyncVar]
        public int firstPlayerIndex;

        //Marked characters. When their turn comes up, the effect is applied
        [SyncVar]
        public int assassinatedChar;
        [SyncVar]
        public int charToSteal;
        [SyncVar]
        public int destroyedDistrict;

        [SyncVar]
        public int roundCount;

        public StringBuilder activityLog;
        public StringBuilder summaryLog;

        public SyncListBool playerListCompleted = new SyncListBool();

        void Awake() {
            instance = this;

            activityLog = new StringBuilder();

            summaryLog = new StringBuilder();

            playerList = new List<PlayerController>();
        }

        public override void OnStartClient() {
            //Once Game Master is done loading, check if other components are done as well
            //Begin initialization once everything is ready
            StartCoroutine(cInitialize());
        }

        //Control initialization of all players and UI
        IEnumerator cInitialize() {
            bool isReady = false;

            while(!isReady) {
                yield return new WaitForSeconds(0.25f);

                //Initialize ready flag
                isReady = true;

                //Check if all players are connected
                isReady &= PlayerController.playerCount >= NetworkManager.singleton.numPlayers;

                //Check if UI Manager has been initialized
                isReady &= UIManager.instance != null;
            }

            //Get all players to initialize and add in list to manage them
            GameObject[] playerGOs = GameObject.FindGameObjectsWithTag(CData.TAG_PLAYER);
            foreach(var playerGO in playerGOs) {
                PlayerController player = playerGO.GetComponent<PlayerController>();
                playerList.Add(player);
                yield return StartCoroutine(player.cGMasterStart());
            }

            //When everything has been loaded properly, initialize UI
            uiManager = UIManager.instance;
            yield return StartCoroutine(uiManager.cGMasterStart());

            if(isServer) {
                //Set variables that determine the game's end before load or new game
                isLastRound = false;
                firstPlayerIndex = -1;

                //Check if resuming from a previous game
                if(CData.isLoadGame) {
                    loadGameMaster();
                } else {
                    initGameMaster();
                }

                StartCoroutine(LateStart());
            }
        }

        //Load data from last save instead
        void loadGameMaster() {
            GameMasterData loadedData = CData.loadedGameMaster;

            districtDeck.Clear();
            foreach(int district in loadedData.districtDeck) {
                districtDeck.Add(district);
            }

            crownHolderI = loadedData.crownHolderI;

            roundCount = loadedData.roundCount;

            transitionPauseDur = loadedData.transitionPauseDur;

            playerListData = loadedData.playerList;
            Debug.Log("--- " + System.Reflection.MethodBase.GetCurrentMethod().Name + " ---");
            Debug.Log("  " + "playerListData: " + playerListData.Count);

            //Sort player data based on player index
            playerListData.Sort(new PlayerDataComparer());

            for(int i = 0; i < playerListData.Count; i++) {
                Debug.Log("  " + i + " " + playerListData[i].playerName + " " + playerListData[i].playerListI + " " + playerListData[i].key);
            }

            activityLog = new StringBuilder();
            activityLog.Append(loadedData.savedActivityLog);
        }

        class PlayerDataComparer : IComparer<PlayerData>
        {
            public int Compare(PlayerData x, PlayerData y) {
                if(x.playerListI > y.playerListI)
                    return 1;
                if(x.playerListI < y.playerListI)
                    return -1;
                else
                    return 0;
            }
        }

        void initGameMaster() {
            //Initialize the district once for the whole game
            initDistrictDeck();

            crownHolderI = (int)Character.Name.None;

            roundCount = 0;

            switch(CData.gameSpeed) {
                case 1:
                    transitionPauseDur = 2.5f;
                    break;
                case 2:
                    transitionPauseDur = 1.75f;
                    break;
                case 3:
                    transitionPauseDur = 1;
                    break;
                default:
                    transitionPauseDur = 1.5f;
                    break;
            }
        }

        //Create/populate Character deck, 1 card per character, then shuffle
        void initCharacterDeck() {
            //Iterate all Characters and place in deck
            foreach(Character.Name character in CData.characters) {
                characterDeck.Add((int)character);
            }
            //Remove null value from Character enum
            characterDeck.Remove((int)Character.Name.None);
        }

        //Create/populate District deck then shuffle, card count varies per District, specified in CData class
        void initDistrictDeck() {
            //Iterate all Districts and place in deck
            foreach(District.Name district in CData.districtNames) {
                //Get specified card count
                int cardCount = CData.getDistrictData(District.Data.Count, district);

                for(int i = 0; i < cardCount; i++)
                    districtDeck.Add((int)district);
            }

            shuffleList(ref districtDeck);
        }

        //Modify list directly with to shuffle
        public void shuffleList(ref SyncListInt deck) {
            int n = deck.Count;

            while(n > 1) {
                n--;
                int k = Random.Range(0, n + 1);
                int value = deck[k];
                deck[k] = deck[n];
                deck[n] = value;
            }
        }

        //Delayed start to ensure that all players are registered in playerList
        IEnumerator LateStart() {
            //Ensure that only GameMaster from server controls game flow
            if(!isServer)
                yield break;


            yield return new WaitUntil(() => PlayerController.playerCount >= NetworkManager.singleton.numPlayers);

            while(playerListCompleted.Count < NetworkManager.singleton.numPlayers) {
                playerListCompleted.Clear();
                RpcSetupOpponentPanels();
                yield return new WaitForSeconds(3);

                Debug.Log("Waiting for players to initialize..");
            }
            Debug.Log("Players initialized!");

            //Skip initialization if game is loaded
            if(!CData.isLoadGame) {
                RpcBroadcastLog("Begin! \n" + playerList.Count + " Player Rule \n");

                //Show tutorial for new players
                RpcShowTutorial(CData.TUTORIAL_GOAL);

                //Give players starting cards and gold
                foreach(var player in playerList) {
                    player.initPlayer();
                }

                yield return new WaitForSeconds(transitionPauseDur);

                crownHolderI = Random.Range(0, playerList.Count);
                RpcBroadcastLog("Selecting the Crown holder..");
                RpcBroadcastLog(playerList[crownHolderI].playerName + " possesses the Crown");
            } else {
                RpcBroadcastLog("Resuming game! \n");



                Debug.Log("... Loading Players ... Count: " + playerList.Count);
                foreach(var player in playerList) {
                    player.loadData();
                }

                yield return new WaitForSeconds(transitionPauseDur);

                RpcBroadcastLog(playerList[crownHolderI].playerName + " possesses the Crown");
            }


            //Start game loop by starting character selection
            StartCoroutine(cCharSelectPhase());
        }

        [ClientRpc]
        public void RpcShowTutorial(int tutorialIndex) {
            if(CData.playerProfile.isTutorial)
                StartCoroutine(uiManager.cShowTutorial(tutorialIndex));
        }

        [ClientRpc]
        void RpcSetupOpponentPanels() {
            uiManager.setupOpponentPanels();
        }

        void hShowCrownHolder(int newCrownHolder) {
            crownHolderI = newCrownHolder;
            uiManager.updateCrown(crownHolderI);
        }

        //Starts character selection for each player
        IEnumerator cCharSelectPhase() {
            //Ensure that this is only called from server
            if(!isServer)
                yield break;

            roundCount++;

            //Allow to close and reopen character select window
            RpcSetupCharSelect();

            RpcBroadcastLog("\nCharacter selection phase begins \n");

            yield return new WaitForSeconds(transitionPauseDur);

            //Hide turn indicator since no characters yet
            RpcShowCharTurn(false);

            //Refresh turn list
            playerCharTurns = new PlayerController[CData.CHARACTER_COUNT];
            clearCharacters();

            initCharacterDeck();

            //First facedown card
            shuffleList(ref characterDeck);
            characterDiscard.Add(characterDeck[0]);
            characterDeck.RemoveAt(0);
            RpcBroadcastLog("A character is faced down");

            //Pause for transition
            yield return new WaitForSeconds(transitionPauseDur);

            //Rules state that certain characters be revealed depending on number of players
            int faceupCards = 0;
            if(playerList.Count == 4)
                faceupCards = 2;
            else if(playerList.Count == 5)
                faceupCards = 1;

            for(int i = 0; i < faceupCards; i++) {
                characterDiscard.Add(characterDeck[0]);
                RpcBroadcastLog("Faceup: " + CData.characters[characterDeck[0]]);
                characterDeck.RemoveAt(0);
            }

            //Start character selection from the player with crown
            int playerIndex = crownHolderI;

            //Players select and pass character cards, different rules are based on player count
            for(int i = 0; i < CData.CHARACTER_SELECT_ROUNDS[playerList.Count]; i++) {
                RpcBroadcastLog(playerList[playerIndex].playerName + "'s turn to choose a character");
                yield return new WaitForSeconds(transitionPauseDur);

                //Wait for player to finish selecting
                PlayerController playerTurn = playerList[playerIndex];
                yield return StartCoroutine(playerTurn.cCharSelectPhase());

                //Assign player's turn according to selected character
                playerCharTurns[playerList[playerIndex].characterFirst] = playerList[playerIndex];
                //Check if player has chosen a second character
                if(playerList[playerIndex].characterSecond != (int)Character.Name.None)
                    playerCharTurns[playerList[playerIndex].characterSecond] = playerList[playerIndex];


                //Proceed to next player
                playerIndex++;
                playerIndex %= playerList.Count;
            }
            //Recreate character turn list on clients
            RpcConstructCharTurnList();

            RpcBroadcastLog("\n" + "Character selection phase ends \n");
            yield return new WaitForSeconds(transitionPauseDur);
            RpcCloseCharSelect();

            StartCoroutine(cDistrictPhase());
        }

        void clearCharacters() {
            for(int i = 0; i < playerList.Count; i++) {
                playerList[i].characterFirst = (int)Character.Name.None;
                playerList[i].characterSecond = (int)Character.Name.None;
            }
        }

        [ClientRpc]
        void RpcUpdateOpponent(int i) {
            uiManager.updateOpponent(i);
        }

        [ClientRpc]
        void RpcSetupCharSelect() {
            uiManager.charSelBtn.gameObject.SetActive(true);
            uiManager.charSelCloseBtn.onClick.AddListener(() => {
                uiManager.resumeCharSelBtn.SetActive(true);
            });
        }

        [ClientRpc]
        void RpcShowCharTurn(bool isShow) {
            uiManager.displayTurnIndicator(isShow);
        }

        [ClientRpc]
        void RpcCloseCharSelect() {
            //Remove capability to reopen character select panel
            uiManager.charSelCloseBtn.onClick.RemoveAllListeners();
            uiManager.resumeCharSelBtn.SetActive(false);
            uiManager.charSelBtn.gameObject.SetActive(false);
        }

        [ClientRpc]
        void RpcConstructCharTurnList() {
            //Reduce Character Turns to avoid Character.Name.None
            playerCharTurns = new PlayerController[CData.characters.Length - 1];

            for(int i = 0; i < playerList.Count; i++) {
                playerCharTurns[playerList[i].characterFirst] = playerList[i];

                if(playerList[i].characterSecond != (int)Character.Name.None)
                    playerCharTurns[playerList[i].characterSecond] = playerList[i];
            }
        }


        //Players follow the order of selected character, build districts and use abilities
        IEnumerator cDistrictPhase() {
            //Ensure that this is only called from server
            if(!isServer)
                yield break;

            initDistrictPhase();

            //Show tutorial for new players
            RpcShowTutorial(CData.TUTORIAL_DIST_PHASE);

            //Iterate through player turns
            for(int i = 0; i < playerCharTurns.Length; i++) {
                curCharacterTurn = i;

                yield return new WaitForSeconds(transitionPauseDur);

                //Skip the assassinated character
                if(i == assassinatedChar) {
                    RpcBroadcastLog("The " + CData.characters[assassinatedChar] + " has been assassinated \n");
                    if(playerCharTurns[i] != null)
                        summaryLog.AppendLine(playerCharTurns[(int)Character.Name.Assassin].playerName +
                        " assassinated " + playerCharTurns[i].playerName + "'s " + CData.characters[assassinatedChar]);
                    continue;
                }

                //Check if no player selected the character, else start the player's turn
                if(playerCharTurns[i] == null) {
                    RpcBroadcastLog("No one hired the " + CData.characters[i] + "\n");
                    yield return new WaitForSeconds(transitionPauseDur);
                } else {
                    RpcUpdateOpponent(playerCharTurns[i].playerListI);

                    //Start and wait for player's turn to finish
                    yield return StartCoroutine(playerCharTurns[i].cDistrictPhase());

                    //Check if anyone has complete districts built
                    if(firstPlayerIndex < 0 && playerCharTurns[i].builtDistricts.Count >= CData.BUILD_COUNT_LIMIT) {
                        firstPlayerIndex = playerCharTurns[i].playerListI;
                        isLastRound = true;
                        RpcBroadcastLog("\n" + playerCharTurns[i].playerName + " has completed a city! \n");
                    }
                    yield return new WaitForSeconds(transitionPauseDur);
                }
            }
            RpcBroadcastLog("District building phase ends");

            getRoundSummary();
            displayRoundSummary();

            RpcShowCharTurn(false);

            //Reset character turns before selecting again
            curCharacterTurn = 0;

            yield return new WaitForSeconds(transitionPauseDur);

            //Show tutorial for new players
            if(CData.playerProfile.isTutorial)
                StartCoroutine(uiManager.cShowTutorial(CData.TUTORIAL_SCORE));

            if(isLastRound)
                gameOver();
            else {
                //Save game progress at the end of the round
                //Only the host/server keeps the saved data
                CData.saveGameMaster();

                //Clients get to save their profile
                RpcSaveProfile();
                StartCoroutine(cCharSelectPhase());
            }
        }

        //Reset round variables
        void initDistrictPhase() {
            curCharacterTurn = 0;

            assassinatedChar = (int)Character.Name.None;
            charToSteal = (int)Character.Name.None;

            //If player's character is the king, get the crown
            //Set crown holder to whoever picked the King early so its not affected by assassination
            if(playerCharTurns[(int)Character.Name.King] != null) {
                PlayerController crownHolder = playerCharTurns[(int)Character.Name.King];
                crownHolderI = crownHolder.playerListI;
                summaryLog.AppendLine(crownHolder.playerName + " possesses the Crown");
            }

            //Show turn indicator
            RpcShowCharTurn(true);
            RpcBroadcastLog("District building phase begins \n");
        }

        void hUpdateTurnIndicator(int newTurnI) {
            curCharacterTurn = newTurnI;
            uiManager.updateTurn(newTurnI);
        }

        //Draw district cards from deck
        public District.Name[] drawDistricts(int count) {
            //Limit draw count with the number of districts left in deck
            count = Mathf.Clamp(count, 0, districtDeck.Count);

            District.Name[] districts = new District.Name[count];

            for(int i = 0; i < count; i++) {
                districts[i] = CData.districtNames[districtDeck[0]];
                districtDeck.RemoveAt(0);
            }
            return districts;
        }

        //Destroy district
        public void destroyDistrict(int playerI, int districtI, int cost) {
            //Remove destroyed district from player's built list
            PlayerController player = playerList[playerI];
            //Mark the destroyed district
            destroyedDistrict = player.builtDistricts[districtI];
            player.builtDistricts.RemoveAt(districtI);

            //Deduct destroy cost to perpetrator
            playerCharTurns[curCharacterTurn].gold -= cost;

            string message = playerCharTurns[curCharacterTurn].playerName + " destroyed " +
                                      player.playerName + "'s " + CData.districtNames[destroyedDistrict] + " for ";
            message += (cost > 0) ? cost.ToString() + " gold" : "free";
            RpcBroadcastLog(message);
            summaryLog.AppendLine(message);

            presentGraveyardChoice();
        }

        void presentGraveyardChoice() {
            //Cannot retrieve Graveyard
            if(destroyedDistrict == (int)District.Name.Graveyard)
                return;

            //Check if a player has built the Graveyard, present choice to buy destroyed district
            foreach(var graveOwner in playerList) {
                if(graveOwner.builtDistricts.Contains((int)District.Name.Graveyard) &&
                               playerCharTurns[(int)Character.Name.Warlord] != graveOwner && graveOwner.gold > 0) {
                    //Wait for player input to take district
                    graveOwner.RpcGraveyardChoice(destroyedDistrict);
                    break;
                }
            }
        }

        void gameOver() {
            //Ensure that only the server will be able to execute this function
            if(!isServer)
                return;

            RpcShowCharTurn(false);
            RpcBroadcastLog("City value tally:");

            int[] scores = new int[playerList.Count];
            string[] names = new string[playerList.Count];
            for(int i = 0; i < playerList.Count; i++) {
                int score = playerList[i].cityScore();

                scores[i] = score;
                names[i] = playerList[i].playerName;

                //Record score if main/local player
                playerList[i].RpcRecordScore(score);
            }

            //Display score tally ascendingly
            System.Array.Sort(scores, names);
            for(int i = 0; i < playerList.Count; i++) {
                RpcBroadcastLog(names[i] + "\t\t" + scores[i]);
            }
            RpcBroadcastLog(names[names.Length - 1] + " claims victory!");

            CData.deleteGameSave(CData.saveFilePath);
            RpcShowMainMenuBtn();
        }

        [ClientRpc]
        void RpcSaveProfile() {
            CData.playerProfile.isTutorial = false;
            CData.saveProfile();
        }

        [ClientRpc]
        void RpcShowMainMenuBtn() {
            uiManager.mainMenuBtn.SetActive(true);
        }

        [Command]
        void CmdBroadcastLog(string message) {
            RpcBroadcastLog(message);
        }

        [ClientRpc]
        public void RpcBroadcastLog(string message) {
            if(activityLog.Length > CData.LOG_CHARACTER_LIMIT)
                activityLog.Remove(0, message.Length);
            activityLog.AppendLine(message);

            uiManager.displayActivity();
        }

        void getRoundSummary() {
            foreach(var player in playerList) {
                if(player.goldGainRound >= CData.HIGH_GOLD_GAIN)
                    summaryLog.AppendLine(player.playerName + " was able to procure " + player.goldGainRound + " gold");

                if(player.builtDistrictRound >= CData.ARCHITECT_BUILD_BONUS + CData.BUILD_COUNT_TURN)
                    summaryLog.AppendLine(player.playerName + " was able to build " + player.builtDistrictRound + " districts");
            }
        }

        public void displayRoundSummary() {
            RpcBroadcastLog("\n * Round " + roundCount + " Summary *");
            RpcBroadcastLog(summaryLog.ToString());
            summaryLog = new StringBuilder();
        }
    }
}