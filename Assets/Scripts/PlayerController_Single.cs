using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Citadel
{
    public class PlayerController_Single : MonoBehaviour
    {
        protected GameMaster_Single gMaster;
        protected UIManager_Single uiManager;

        public int savedDataKey;
        //Placement in GameMaster_Single turn list
        public int playerListI;
        public string playerName;

        //Number of district that can be built during turn
        public int buildCount;
        public int gold;

        public List<District.Name> hand;
        public List<District.Name> builtDistricts;

        public Character.Name characterFirst;
        public Character.Name characterSecond;

        public bool isWaitInput;
        public bool isLocalPlayer;
        public bool isAbilityUsed;
        public bool isLaboratoryUsed;
        public bool isSmithyUsed;

        public int goldGainRound;
        public int builtDistrictRound;

        void Awake() {
            setPlayerName();
        }

        void Start() {
            gMaster = GameMaster_Single.instance;
            uiManager = UIManager_Single.instance;

            characterFirst = Character.Name.None;
            characterSecond = Character.Name.None;
            uiManager.updatePlayerCharacter();
            uiManager.setPlayerName();

            //Skip initialize when loaded
            if(CData.isLoadGame) {
                loadData();
                return;
            }

            gMaster.playerList.Add(this);

            builtDistricts = new List<District.Name>();

            initHand();

            gold = CData.GOLD_START_COUNT;
            uiManager.updatePlayerGold();
            uiManager.sfxSource.PlayOneShot(uiManager.goldGain);
        }

        public void loadData() {
            foreach(var player in gMaster.playerListData) {
                if(player.key == savedDataKey) {
                    playerListI = player.playerListI;
                    playerName = player.playerName;
                    gold = player.gold;
                    hand = new List<District.Name>();
                    foreach(var district in player.hand) {
                        hand.Add(CData.districtNames[district]);
                    }

                    //Register self to Player List so Game Master can process player
                    gMaster.playerList.Add(this);

                    builtDistricts = new List<District.Name>();
                    foreach(var district in player.builtDistricts) {
                        builtDistricts.Add(CData.districtNames[district]);
                    }
                    uiManager.updatePlayerGold();
                    uiManager.updateBuiltDistricts();
                    break;
                }
            }
        }

        //Initialize hand and give starting district cards from district deck
        void initHand() {
            hand = new List<District.Name>();
            hand.AddRange(gMaster.drawDistricts(CData.HAND_START_COUNT));
        }

        //Set name in a method to be overridden easily
        protected void setPlayerName() {
            playerName = CData.playerProfile.playerName;
            isLocalPlayer = true;
            savedDataKey = CData.playerProfile.CITADELS_DATA_KEY;
        }

        public virtual IEnumerator cCharSelectPhase() {
            goldGainRound = 0;
            builtDistrictRound = 0;

            //Seven Player Rule, the last player can select between last character and first discarded character
            if(gMaster.characterDeck.Count == 1 && gMaster.playerList.Count == 7) {
                gMaster.characterDeck.Add(gMaster.characterDiscard[0]);
                gMaster.characterDiscard.RemoveAt(0);
            }

            //Activate Character Select Panel
            uiManager.initCharSelect("Select a character");
            uiManager.setupChar(gMaster.characterDeck.ToArray(), true);

            //Show tutorial for new players
            if(CData.playerProfile.isTutorial)
                StartCoroutine(uiManager.cShowTutorial(CData.TUTORIAL_CHAR_SEL));

            //Suspend execution while player is selecting
            yield return StartCoroutine(cWaitForInput());

            //Retrieve player input from UIManager_Single
            int selected = uiManager.selectedCharacterI;

            //Select a character from the available choices
            if(characterFirst == Character.Name.None) {
                characterFirst = CData.characters[selected];
                gMaster.characterDeck.Remove(characterFirst);
            }
            //Two and Three-player Rule, all players use 2 Characters
            else if(gMaster.playerList.Count <= 3 && characterFirst != Character.Name.None) {
                characterSecond = CData.characters[selected];
                gMaster.characterDeck.Remove(characterSecond);
            }

            //Two-Player Rule, need to discard when selecting a character
            if(gMaster.playerList.Count == 2) {
                //The first player to select character does not discard
                if(gMaster.characterDeck.Count < CData.CHARACTER_COUNT - 2) {
                    //Choose another time for discard
                    uiManager.initCharSelect("Discard a character");
                    uiManager.setupChar(gMaster.characterDeck.ToArray(), true);

                    yield return StartCoroutine(cWaitForInput());

                    selected = uiManager.selectedCharacterI;

                    gMaster.characterDiscard.Add(CData.characters[selected]);
                    gMaster.characterDeck.Remove(CData.characters[selected]);
                }
            }

            //Update player UI to display selection
            uiManager.updatePlayerCharacter();
        }

        public virtual IEnumerator cDistrictPhase() {
            onStartTurn();

            //Select Action
            uiManager.showActionSelect();

            //Show tutorial for new players
            if(CData.playerProfile.isTutorial)
                StartCoroutine(uiManager.cShowTutorial(CData.TUTORIAL_PLAYER_TURN));

            yield return StartCoroutine(cWaitForInput());

            //Allow player's other actions
            uiManager.showActionBtns(true);

            yield return StartCoroutine(cWaitForInput());

            yield return StartCoroutine(cOnEndTurn());
        }

        IEnumerator cWaitForInput() {
            isWaitInput = true;
            yield return new WaitWhile(() => isWaitInput);
        }

        //Apply effects on start of player's turn
        protected void onStartTurn() {
            isAbilityUsed = false;
            isSmithyUsed = false;
            isLaboratoryUsed = false;

            uiManager.log(playerName + " hired the " + CData.characters[gMaster.curCharacterTurn]);

            //If player's character is the king, get the crown
            if((int)Character.Name.King == gMaster.curCharacterTurn) {
                uiManager.updateCrown();
            }

            //Check if character is marked for steal
            if((int)gMaster.charToSteal == gMaster.curCharacterTurn) {
                string message = gMaster.playerCharTurns[(int)Character.Name.Thief].playerName +
                    " stole " + gold + " gold from " + playerName;
                uiManager.log(message);

                if(gold >= 2)
                    uiManager.addSummary(message);

                //Transfer gold from victim to player who used thief
                gMaster.playerCharTurns[(int)Character.Name.Thief].gold += gold;
                gMaster.playerCharTurns[(int)Character.Name.Thief].goldGainRound += gold;
                gold = 0;

                uiManager.sfxSource.PlayOneShot(uiManager.thiefTarget);
                uiManager.updatePlayerGold();
                uiManager.updateOpponent(playerListI);
            }

            //If player's character is the merchant, receive a gold
            if((int)Character.Name.Merchant == gMaster.curCharacterTurn) {
                gold++;
                goldGainRound++;

                uiManager.sfxSource.PlayOneShot(uiManager.merchantCoin);
                uiManager.updatePlayerGold();
                uiManager.updateOpponent(playerListI);
                uiManager.log(playerName + " receives one gold, a penny for your thoughts");
            }

            //Reset player's number of allowable districts to build
            buildCount = CData.BUILD_COUNT_TURN;

            //If player's character is the architect, receive bonus districts and can build multiple times
            if((int)Character.Name.Architect == gMaster.curCharacterTurn) {
                if(gMaster.districtDeck.Count > 0)
                    hand.AddRange(gMaster.drawDistricts(CData.ARCHITECT_DRAW_BONUS));
                buildCount += CData.ARCHITECT_BUILD_BONUS;
                uiManager.sfxSource.PlayOneShot(uiManager.architectScribble);
                uiManager.log(playerName + " receives " + CData.ARCHITECT_DRAW_BONUS + " districts, can build multiple times");
            }

            if((int)Character.Name.Bishop == gMaster.curCharacterTurn) {
                uiManager.sfxSource.PlayOneShot(uiManager.bishopBell);
            }
        }

        IEnumerator cOnEndTurn() {
            //Show Warlord ability UI
            if((int)Character.Name.Warlord == gMaster.curCharacterTurn) {
                uiManager.showDestroyPnl();
                yield return StartCoroutine(cWaitForInput());
            }

            uiManager.log(playerName + " End Turn \n");
            uiManager.showActionBtns(false);
        }

        //Compute player score based on districts built
        public int cityScore() {
            int score = 0;

            //Sum of district costs
            foreach(District.Name district in builtDistricts) {
                score += CData.getDistrictData(District.Data.Cost, district);
            }

            //Bonus for having complete districts
            if(builtDistricts.Count >= CData.BUILD_COUNT_LIMIT)
                score += CData.BONUS_COMPLETION;

            //Bonus for being the first one to complete
            if(gMaster.firstCompleteDist == this)
                score += CData.BONUS_FIRST;

            //Bonus for building a district for each type
            bool[] builtTypes = new bool[CData.districtTypes.Length];
            for(int i = 0; i < builtDistricts.Count; i++) {
                builtTypes[getTypeIndex(builtDistricts[i])] = true;
            }

            int typeBuiltCtr = 0;
            foreach(bool builtType in builtTypes) {
                if(builtType)
                    typeBuiltCtr++;
            }

            if(builtDistricts.Contains(District.Name.HauntedCity))
                typeBuiltCtr++;

            if(typeBuiltCtr >= builtTypes.Length) score += CData.BONUS_ALL_TYPES;

            if(builtDistricts.Contains(District.Name.DragonGate))
                score += CData.BONUS_DRAGON_GATE;

            if(builtDistricts.Contains(District.Name.University))
                score += CData.BONUS_UNIVERSITY;

            return score;
        }

        protected int getTypeIndex(District.Name card) {
            District.Type[] types = (District.Type[])System.Enum.GetValues(typeof(District.Type));

            for(int j = 0; j < types.Length; j++) {
                if((int)card <= (int)types[j])
                    return j;
            }
            return -1;
        }

        //Choose a character, steal all their turn on start of their turn
        public virtual IEnumerator steal() {
            uiManager.initCharSelect("Steal from a character");

            //Thief cannot steal from himself, the assassin, and the assassin's target

            Character.Name[] exception = {
            Character.Name.Assassin, Character.Name.Thief, gMaster.assassinatedChar, characterFirst, characterSecond};
            uiManager.setupChar(exception, false);

            yield return StartCoroutine(cWaitForInput());

            //Mark the character to steal from
            gMaster.charToSteal = CData.characters[uiManager.selectedCharacterI];
            uiManager.sfxSource.PlayOneShot(uiManager.thiefSteal);
            uiManager.log(playerName + " marked the " + gMaster.charToSteal + " for theft");
        }

        //Choose an opponent, swap their hand with yours
        public virtual void exchangePlayer(int opponentI) {
            PlayerController_Single opponent = gMaster.playerList[opponentI];
            var temp = hand;
            hand = opponent.hand;
            opponent.hand = temp;

            uiManager.updateOpponent(opponentI);
            uiManager.abilityBtn.interactable = false;
            isAbilityUsed = true;
            uiManager.onPressMagicianClose();
            uiManager.sfxSource.PlayOneShot(uiManager.magicianExchange);
            string message = playerName + " exchanged districts with " + opponent.playerName;
            uiManager.log(message);
            uiManager.addSummary(message);
        }

        //Select cards from hand, return to district deck and get same amount in 
        public virtual void exchangeDeck(List<int> exchangeList) {
            uiManager.sfxSource.PlayOneShot(uiManager.magicianExchange);
            uiManager.log(playerName + " exchanged " + exchangeList.Count + " districts back to deck");

            //Return selected cards to deck
            District.Name[] districts = new District.Name[exchangeList.Count];
            for(int i = 0; i < exchangeList.Count; i++) {
                districts[i] = hand[exchangeList[i]];
                gMaster.returnDistrict(hand[exchangeList[i]]);
            }
            //Remove selected cards from hand in a separate loop to avoid messing indices
            for(int i = 0; i < districts.Length; i++) {
                hand.Remove(districts[i]);
            }

            //Draw same amount that was returned
            hand.AddRange(gMaster.drawDistricts(exchangeList.Count));
        }

        int taxAmount;
        public int getTaxAmount(District.Type taxedType) {
            taxAmount = 0;

            //Iterate through built districts and get gold for matching type
            foreach(District.Name district in builtDistricts) {
                if(CData.getCardType(district) == taxedType)
                    taxAmount++;
            }

            if(builtDistricts.Contains(District.Name.SchoolOfMagic)) {
                uiManager.log(playerName + " used " + District.Name.SchoolOfMagic + ", gained additional gold tax");
                taxAmount++;
            }
            return taxAmount;
        }

        //Get the district type affiliated with current character, get a gold for each district built of said type
        public virtual void taxDistricts(District.Type taxedType) {
            string message = playerName + " taxed " + taxedType + " districts, ";
            if(taxAmount > 0) {
                gold += taxAmount;
                goldGainRound += taxAmount;

                uiManager.sfxSource.PlayOneShot(uiManager.goldGain);
                uiManager.updatePlayerGold();
                message += "gained " + taxAmount + " gold";
            } else {
                message += "but doesn't have such districts :(";
            }
            uiManager.log(message);
            uiManager.abilityBtn.interactable = false;
            isAbilityUsed = true;
        }

        public virtual void graveyardChoice(District.Name district) {
            uiManager.showChoice("Buy the destroyed district for one gold?", uiManager.DistrictCards[(int)district]);
            uiManager.choiceYesBtn.onClick.RemoveAllListeners();
            uiManager.choiceYesBtn.onClick.AddListener(buyDestroyedDistrict);
        }

        //Salvage district
        public virtual void buyDestroyedDistrict() {
            gold--;
            hand.Add(gMaster.destroyedDistrict);
            uiManager.sfxSource.PlayOneShot(uiManager.districtCoins);
            uiManager.updatePlayerGold();
            uiManager.updateOpponent(playerListI);
            uiManager.log(playerName + " used " + District.Name.Graveyard + ", salvaged " + gMaster.destroyedDistrict);
        }

        //Pay gold to gain districts
        public void smithy() {
            if(gMaster.districtDeck.Count <= 0)
                return;
            hand.AddRange(gMaster.drawDistricts(CData.SMITHY_DRAW));
            gold -= CData.SMITHY_COST;
            uiManager.sfxSource.PlayOneShot(uiManager.districtCoins);
            uiManager.updatePlayerGold();
            uiManager.updateOpponent(playerListI);
            isSmithyUsed = true;
            uiManager.smithyBtn.interactable = false;
            uiManager.log(playerName + " used " + District.Name.Smithy + ", obtained " + CData.SMITHY_DRAW + " districts");
        }

        //Discard a district to gain gold
        public void laboratory(District.Name district) {
            gMaster.returnDistrict(district);
            hand.Remove(district);
            gold++;
            goldGainRound++;

            uiManager.sfxSource.PlayOneShot(uiManager.districtCoins);
            uiManager.updatePlayerGold();
            uiManager.updateOpponent(playerListI);
            isLaboratoryUsed = true;
            uiManager.log(playerName + " used " + District.Name.Laboratory + ", traded a districts for a gold");
        }
    }
}