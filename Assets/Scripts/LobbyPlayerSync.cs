using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;

public class LobbyPlayerSync : LobbyHook
{
    //Handles the transition from Lobby to Game scene
    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer) {
        //Player's entry in multiplayer lobby
        LobbyPlayer lPlayer = lobbyPlayer.GetComponent<LobbyPlayer>();

        //Player's game object that will be used in Game Scene
        Citadel.PlayerController gPlayer = gamePlayer.GetComponent<Citadel.PlayerController>();

        //If resuming from a saved game, set loaded data values
        if(Citadel.CData.isLoadGame) {
            System.Collections.Generic.List<Citadel.PlayerData> playerData = Citadel.CData.loadedGameMaster.playerList;

            for(int i = 0; i < playerData.Count; i++) {
                //Find the saved data by matching data keys
                if(playerData[i].key == lPlayer.playerDataKey) {
                    //Set name and the order in game list so player will be able to load the right data in game scene
                    gPlayer.playerName = playerData[i].playerName;
                    gPlayer.playerListI = playerData[i].playerListI;
                }
            }
        } else {
            //If new game then set data from lobby player to game scene object
            gPlayer.playerName = lPlayer.playerName;

            for(int i = 0; i < LobbyPlayerList._instance._players.Count; i++) {
                if(LobbyPlayerList._instance._players[i] == lPlayer)
                    //Follow player order in lobby in game scene
                    gPlayer.playerListI = i;
            }
        }
    }
}
