using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.Generic;

namespace District
{
    public enum Name
    {
        Watchtower, Prison, Battlefield, Fortress,
        Manor, Castle, Palace,
        Tavern, Market, TradingPost, Docks, Harbor, TownHall,
        Temple, Church, Monastary, Cathedral,
        HauntedCity, Keep, Laboratory, Smithy, Observatory, Graveyard, DragonGate, University, Library, GreatWall, SchoolOfMagic,
        //Lighthouse, Armory, Museum, ImperialTreasury, MapRoom, WishingWell, Quarry, PoorHouse, BellTower, Factory, Park, Hospital, ThroneRoom
    }

    public enum Type
    {
        Military = 3, Noble = 6, Trade = 12, Religious = 16, Special = 27
    }

    public enum Data
    {
        Count, Cost
    }
}

namespace Character
{
    public enum Name
    {
        None = -1, Assassin, Thief, Magician, King, Bishop, Merchant, Architect, Warlord
    }
}

namespace Citadel
{
    [System.Serializable]
    public class GameMasterData
    {
        public int aiCount;

        public float transitionPauseDur;

        public List<PlayerData> playerList;

        public int crownHolderI;

        public int[] districtDeck;

        public int destroyedDistrict;

        public int roundCount;

        public string savedActivityLog;

        public string timeCreated;
        public bool isMultiplayer;

        //Stores game master data to save game progress and be loaded at a later time
        public GameMasterData(GameMaster gmSave) {
            aiCount = 0;

            transitionPauseDur = gmSave.transitionPauseDur;

            playerList = new List<PlayerData>();

            //Use keys to know which data a player should load
            UnityEngine.Debug.Log("--- " + System.Reflection.MethodBase.GetCurrentMethod().Name + " ---");
            foreach(var player in gmSave.playerList) {
                playerList.Add(new PlayerData(player, player.dataKey));
                UnityEngine.Debug.Log("  " + "playerList: " + playerList.Count + " " + playerList[playerList.Count - 1].playerName);
            }

            crownHolderI = gmSave.crownHolderI;

            districtDeck = new int[gmSave.districtDeck.Count];
            gmSave.districtDeck.CopyTo(districtDeck, 0);

            roundCount = gmSave.roundCount;

            savedActivityLog = gmSave.activityLog.ToString();

            //Save the time of creation
            System.DateTime timeNow = System.DateTime.Now;
            timeCreated = System.String.Format("{0:M/d HH:mm}", timeNow);

            isMultiplayer = CData.isMultiplayer;
        }

        public GameMasterData(GameMaster_Single gmSave) {
            aiCount = gmSave.aiCount;

            transitionPauseDur = gmSave.transitionPauseDur;

            playerList = new List<PlayerData>();

            //Use keys to know which data a player should load
            UnityEngine.Debug.Log("--- " + System.Reflection.MethodBase.GetCurrentMethod().Name + " ---");
            foreach(var player in gmSave.playerList) {
                playerList.Add(new PlayerData(player, player.savedDataKey));
                UnityEngine.Debug.Log("  " + "playerList: " + playerList.Count + " " + playerList[playerList.Count - 1].playerName);
            }

            crownHolderI = gmSave.crownHolderI;

            districtDeck = new int[gmSave.districtDeck.Count];
            //Indexing is not applicable to Queue
            District.Name[] temp = gmSave.districtDeck.ToArray();
            for(int i = 0; i < gmSave.districtDeck.Count; i++) {
                districtDeck[i] = (int)temp[i];
            }

            roundCount = gmSave.roundCount;
            savedActivityLog = gmSave.savedActivityLog;

            //Save the time of creation
            System.DateTime timeNow = System.DateTime.Now;
            timeCreated = System.String.Format("{0:M/d HH:mm}", timeNow);

            isMultiplayer = CData.isMultiplayer;
        }
    }

    [System.Serializable]
    //Stores player data to be saved and loaded at a later time
    public class PlayerData
    {
        public int key;

        public int playerListI;
        public string playerName;

        public int gold;

        public int[] hand;
        public int[] builtDistricts;

        public PlayerData(PlayerController player, int key) {
            playerListI = player.playerListI;
            playerName = player.playerName;
            gold = player.gold;

            hand = new int[player.hand.Count];
            player.hand.CopyTo(hand, 0);

            builtDistricts = new int[player.builtDistricts.Count];
            player.builtDistricts.CopyTo(builtDistricts, 0);

            this.key = key;
        }

        public PlayerData(PlayerController_Single player, int key) {
            playerListI = player.playerListI;
            playerName = player.playerName;
            gold = player.gold;

            hand = new int[player.hand.Count];
            for(int i = 0; i < player.hand.Count; i++) {
                hand[i] = (int)player.hand[i];
            }

            builtDistricts = new int[player.builtDistricts.Count];
            for(int i = 0; i < player.builtDistricts.Count; i++) {
                builtDistricts[i] = (int)player.builtDistricts[i];
            }

            this.key = key;
        }
    }

    [System.Serializable]
    public class PlayerProfile
    {
        //Used by human user to retrieve game save
        public int CITADELS_DATA_KEY;
        public string playerName;
        public int totalScore;
        public int highScore;
        public bool isTutorial;

        public float bgmVol;
        public float sfxVol;

        public PlayerProfile() {
            //Get time of profile creation to make a unique key
            System.DateTime timeNow = System.DateTime.Now;
            int dataKey = (timeNow.Month * 10000) + (timeNow.Day * 1000) + (timeNow.Hour * 100) + timeNow.Minute;
            CITADELS_DATA_KEY = dataKey;

            playerName = "";
            totalScore = 0;
            highScore = 0;
            isTutorial = true;

            bgmVol = 1;
            sfxVol = 1;

            CData.playerProfile = this;
        }
    }

    //Class to access various game data/mechanic
    public static class CData
    {
        /*
         * SAVED / PERSISTENT DATA
         */

        public static PlayerProfile playerProfile;
        public static string savedFileToLoad;
        public static GameMasterData loadedGameMaster;

        public static bool isLoadGame;
        public static bool isMultiplayer;
        public static int opponentCount;
        public static int gameSpeed;

        public static string saveFilePath;
        public const string SAVE_DIRECTORY = "/Saves";
        public const string FILE_EXTENSION_SINGLE = ".cts";
        public const string FILE_EXTENSION_MULTI = ".ctm";
        public const string FILE_PATH_SINGLE = "/Saves/SinglePlayerGame.cts";
        public const string FILE_PATH_PROFILE = "profile.ct";
        public const int SCENE_MAIN_MENU = 0;
        public const int SCENE_GAME_MULTI = 1;
        public const int SCENE_GAME_SINGLE = 2;

        public static string systemPersistentPath = UnityEngine.Application.persistentDataPath;

        public static bool isProfileSaveExists() {
            return File.Exists(systemPersistentPath + FILE_PATH_PROFILE);
        }

        public static void saveProfile() {
            try {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream saveFile = File.Create(systemPersistentPath + FILE_PATH_PROFILE);

                formatter.Serialize(saveFile, playerProfile);

                saveFile.Close();
            } catch(System.Exception e) {
                UnityEngine.Debug.Log("Saving Profile Error:\n" + e);
                throw;
            }
        }

        public static void loadProfile() {
            try {
                BinaryFormatter formatter = new BinaryFormatter();
                FileStream saveFile = File.Open(systemPersistentPath + FILE_PATH_PROFILE, FileMode.Open);

                playerProfile = (PlayerProfile)formatter.Deserialize(saveFile);

                saveFile.Close();
            } catch(System.Exception e) {
                UnityEngine.Debug.Log("Loading Profile Error:\n" + e);
                throw;
            }
        }

        public static void deletePlayerProfile() {
            File.Delete(systemPersistentPath + FILE_PATH_PROFILE);
        }

        public static string[] getSavedGames() {
            if(!Directory.Exists(systemPersistentPath + SAVE_DIRECTORY))
                Directory.CreateDirectory(systemPersistentPath + SAVE_DIRECTORY);

            return Directory.GetFiles(systemPersistentPath + SAVE_DIRECTORY); ;
        }

        public static void saveGameMaster() {
            try {
                if(!Directory.Exists(systemPersistentPath + SAVE_DIRECTORY))
                    Directory.CreateDirectory(systemPersistentPath + SAVE_DIRECTORY);

                BinaryFormatter formatter = new BinaryFormatter();

                FileStream saveFile = File.Create(saveFilePath); ;

                GameMasterData gmData;
                if(isMultiplayer)
                    gmData = new GameMasterData(GameMaster.instance);
                else
                    gmData = new GameMasterData(GameMaster_Single.instance);

                formatter.Serialize(saveFile, gmData);

                saveFile.Close();

            } catch(System.Exception e) {
                UnityEngine.Debug.Log("Saving Game Error:\n" + e);
                throw;
            }
        }

        public static GameMasterData loadGameMaster(string filePath) {
            try {
                BinaryFormatter formatter = new BinaryFormatter();

                //Set the saved file to load from main menu, before starting the game scene
                FileStream saveFile = File.Open(filePath, FileMode.Open);

                GameMasterData gmData = (GameMasterData)formatter.Deserialize(saveFile);

                saveFile.Close();

                return gmData;
            } catch(System.Exception e) {
                UnityEngine.Debug.Log("Loading Game Error:\n" + e);
                throw;
            }
        }

        public static void deleteGameSave(string path) {
            try {
                if(File.Exists(path)) {
                    File.Delete(path);
                    UnityEngine.Debug.Log("  " + "Deleted: " + path);
                }
            } catch(System.Exception e) {
                UnityEngine.Debug.Log("  " + e);
                throw;
            }
        }

        //Generate file name to identify the game save 
        public static string generateFileName() {
            //Start file name by indicating the directory
            string fileName = SAVE_DIRECTORY + "/";

            //Get time of profile creation to make a unique key
            System.DateTime timeNow = System.DateTime.Now;
            fileName += (timeNow.Month * 10000) + (timeNow.Day * 1000) + (timeNow.Hour * 100) + timeNow.Minute;

            //Set extension to identify file as a multiplayer game;
            fileName += FILE_EXTENSION_MULTI;

            return fileName;
        }

        /*
         * GAME DATA
         */

        //Convenient way to retrieve character and district using index
        public static Character.Name[] characters = (Character.Name[])System.Enum.GetValues(typeof(Character.Name));
        public static District.Name[] districtNames = (District.Name[])System.Enum.GetValues(typeof(District.Name));
        public static District.Type[] districtTypes = (District.Type[])System.Enum.GetValues(typeof(District.Type));

        static int[,] DISTRICT_DATA = {
        //Total count/number of cards in deck
        { 3, 3, 3, 2, 5, 4, 3, 5, 4, 3, 3, 3, 2, 3, 3, 3, 2, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
        //Cost to build
        { 1, 2, 3 ,5 ,3 ,4 ,5, 1, 2, 2, 3, 4, 5, 1, 2, 3, 5, 2, 3, 5, 5, 5, 5, 6, 6, 6, 6, 6}
    };

        public static int getDistrictData(District.Data data, District.Name district) {
            return DISTRICT_DATA[(int)data, (int)district];
        }

        public const string TAG_PLAYER = "Player";

        public const int HAND_START_COUNT = 4;
        public const int GOLD_START_COUNT = 2;
        public const int CHARACTER_COUNT = 8;
        public const int MAX_OPPONENT_COUNT = 6;
        public const int ACTION_GOLD_GAIN = 2;
        public const int ACTION_DISTRICT_CHOICE = 2;
        public const int ACTION_DISTRICT_HAND = 1;
        public const int SMITHY_COST = 2;
        public const int SMITHY_DRAW = 3;
        public const int BUILD_COUNT_TURN = 1;
        public const int BUILD_COUNT_BONUS = 2;
        public const int BUILD_COST_MAX = 6;
        public const int ARCHITECT_DRAW_BONUS = 2;
        public const int ARCHITECT_BUILD_BONUS = 2;
        public const int BUILD_COUNT_LIMIT = 8;
        public const int BONUS_ALL_TYPES = 3;
        public const int BONUS_FIRST = 2;
        public const int BONUS_COMPLETION = 2;
        public const int BONUS_DRAGON_GATE = 2;
        public const int BONUS_UNIVERSITY = 2;
        public const int HIGH_GOLD_GAIN = 6;
        public const int HIGH_SCORE_CARD_MOD = 48;
        public const int LOG_CHARACTER_LIMIT = 1024;

        public const int SCENE_INDEX_GAME = 1;
        public const int SCENE_INDEX_MENU = 0;

        public const int TUTORIAL_GOAL = 0;
        public const int TUTORIAL_CHAR_SEL = 1;
        public const int TUTORIAL_DIST_PHASE = 2;
        public const int TUTORIAL_PLAYER_TURN = 3;
        public const int TUTORIAL_CHAR_ABILITY = 4;
        public const int TUTORIAL_DIST_BUILD = 5;
        public const int TUTORIAL_LOOP = 6;
        public const int TUTORIAL_SCORE = 7;

        //Number of time players pass the character cards for selection
        public static int[] CHARACTER_SELECT_ROUNDS = {
        0, 0, //Invalid 0 and 1 player game
        4, //Two-Player game passes character cards 4x
        6, //Three-Player game passes character cards 6x
        4, 5, 6, 7 //4 - 7 Player game, players have 1 turn each
    };

        public static District.Type getCardType(District.Name card) {
            for(int j = 0; j < districtTypes.Length; j++) {
                if((int)card <= (int)districtTypes[j])
                    return districtTypes[j];
            }
            return District.Type.Military;
        }

        //Child components of an opponent panel
        public enum OpponentPanel
        {
            Name, Gold, Hand, District, Character1, Character2
        }

        public static int nameGetCount = 0;
        public static string[] AI_NAMES = {
        "Abbadon",
        "Adramalech",
        "Ahpuch",
        "Ahriman",
        "Amon",
        "Angra",
        "Mainyu",
        "Apollyon",
        "Asmodeus",
        "Astaroth",
        "Azazel",
        "Baalberith",
        "Balaam",
        "Baphomet",
        "Bast",
        "Beelzebub",
        "Behemoth",
        "Beherit",
        "Bile",
        "Chemosh",
        "Cimeries",
        "Dagon",
        "Damballa",
        "Demogorgon",
        "Diabolous",
        "Dracula",
        "Emma-O",
        "Erlik",
        "Euronymous",
        "Fenris",
        "Gorgo",
        "Guayota",
        "Haborym",
        "Hades",
        "Hecate",
        "Ishtar",
        "Leviathan",
        "Lilith",
        "Loki",
        "Mammon",
        "Mania",
        "Mantus",
        "Marduk",
        "Mastema",
        "Melek",
        "Taus",
        "Mephistopheles",
        "Metztli",
        "Mictian",
        "Midgard",
        "Milcom",
        "Moloch",
        "Mormo",
        "Naamah",
        "Nergal",
        "Nihasa",
        "Nija",
        "O-Yama",
        "Pan",
        "Pluto",
        "Prosperine",
        "Pwcca",
        "Rimmon",
        "Sabazios",
        "Saitian",
        "Samael",
        "Samnu",
        "Sedit",
        "Sekhmet",
        "Set",
        "Shaitan",
        "Supay",
        "T'an-mo",
        "Tchort",
        "Tezcatlipoca",
        "Thamuz",
        "Tunrida",
        "Typhon",
        "Yaotzin",
        "Yama",
        "Yen-lo-Wang",
    };
    }
}