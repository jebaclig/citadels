using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine;

namespace Citadel
{
    public class UIManager : MonoBehaviour
    {
        public static UIManager instance;
        public GameMaster gMaster;
        public PlayerController playerCon;

        [Space]
        [Header("Player Info Side Panel")]
        public Text playerNameTxt;
        public Image characterFirstImg;
        public Image characterSecondImg;
        public Text goldTxt;

        [Space]
        [Header("Opponents Info")]
        public RectTransform opponentPnl;
        public GameObject[] opponentGos;
        [Tooltip("Size if player count > 3")]
        public Vector2 singleCharPnlSize;
        [Tooltip("Size if player count <= 3")]
        public Vector2 doubleCharPnlSize;
        public int selectedOpponentI;

        [Space]
        [Header("Player Action Buttons")]
        public Button laboratoryBtn;
        public Button museumBtn;
        public Button armoryBtn;
        public Button smithyBtn;
        public Button abilityBtn;
        public Button buildBtn;
        public Button endBtn;

        [Space]
        [Header("Turn Indicator")]
        public Image turnIndicatorImg;
        public Text turnNumTxt;
        public Text turnCharTxt;
        public Text turnPlayerTxt;

        [Space]
        [Header("Activity Log")]
        public RectTransform activityLogRect;
        public GameObject activityLogShowPnl;
        public Text activityLogTxt;
        public Scrollbar activityLogScroll;
        public Image logImg;
        public Color logClr;
        [Range(0, 5)]
        public float logFlashDur = 2;
        public float scrollSpeed = 2.5f;
        float orgActLogHeight = 0;

        [Space]
        [Header("Built Districts")]
        public RectTransform builtDistrictPnl;
        public Button buildSelectedBtn;

        [Space]
        [Header("Character Select")]
        public GameObject charSelPnl;
        public Text charSelDescTxt;
        public Image charSelDisplay;
        public Button[] charSelIcons;
        public Button charSelCloseBtn;
        public GameObject resumeCharSelBtn;
        public Button charSelBtn;
        public Button assassinateBtn;
        public Button stealBtn;

        [Space]
        [Header("Hand Viewer")]
        public GameObject handPanel;
        public RectTransform handGrid;
        public Image handCardImg;
        public Text handDescTxt;
        public GameObject cardPrefab;
        public int selectedDistrictI;

        [Space]
        [Header("Card Viewer")]
        public GameObject singleCardPnl;

        [Space]
        [Header("Action Select")]
        public GameObject actionPanel;
        public Text actionDescTxt;
        public GameObject actionGoldBtn;
        public GameObject actionDistBtn;
        public RectTransform actionDistPnl;
        public Text districtCountTxt;
        int districtDrawCount;
        int districtHandCount;
        District.Name[] cards;

        [Space]
        [Header("Magician Ability")]
        public GameObject magicianPnl;
        public Text magicianDescTxt;
        public GameObject exchangePlayerBtn;
        public GameObject exchangeDeckBtn;
        public Button exchangeSelectCardBtn;
        public Button exchangeSelectionBtn;
        List<int> exchangeList;
        public bool isExchangeAbility;

        [Space]
        [Header("Warlord Ability")]
        public GameObject destroyPnl;
        public Button destroyBtn;
        Button opponentBtn;
        bool isDestroyAbility;

        [Space]
        [Header("Choice Select")]
        public Text choiceDescTxt;
        public Image choiceDisplayImg;
        public Button choiceYesBtn;
        public Button choiceNoBtn;
        public GameObject choicePnl;
        public GameObject tradeGoldBtn;

        [Space]
        [Header("Tutorial")]
        public GameObject tutorialPnl;
        public GameObject[] tutorialTxts;

        [Header("Card Sprites")]
        public Sprite[] CharacterCards;
        public Sprite[] CharacterIcons;
        public Sprite[] DistrictCards;
        public Sprite cardBackSpr;

        [Space]
        [Header("District Colors")]
        public Color neutralClr;
        public Color militaryClr;
        public Color nobleClr;
        public Color tradeClr;
        public Color religiousClr;
        public Color specialClr;
        public Color crownClr;

        [Space]
        [Header("Sounds")]
        public Slider bgmVolSlider;
        public Slider sfxVolSlider;
        public AudioSource sfxSource;
        public AudioSource bgmSource;
        public AudioClip opponentBuild;
        public AudioClip buildSfx;
        public AudioClip opponentGold;
        public AudioClip goldGain;
        public AudioClip districtCoins;
        public AudioClip assassinKill;
        public AudioClip thiefSteal;
        public AudioClip thiefTarget;
        public AudioClip magicianExchange;
        public AudioClip kingFanfare;
        public AudioClip bishopBell;
        public AudioClip merchantCoin;
        public AudioClip architectScribble;
        public AudioClip warlordDestroy;

        [Space]
        public GameObject mainMenuBtn;
        public GameObject optionPnl;

        void Awake() {
            instance = this;

            hideUI();

            bgmVolSlider.value = CData.playerProfile.bgmVol;
            sfxVolSlider.value = CData.playerProfile.sfxVol;
        }

        public IEnumerator cGMasterStart() {
            gMaster = GameMaster.instance;

            setPlayerName(CData.playerProfile.playerName);

            yield return null;
        }

        void hideUI() {
            setCharSprite(characterFirstImg, null);
            setCharSprite(characterSecondImg, null);

            goldTxt.text = "0";

            orgActLogHeight = activityLogRect.rect.height;

            displayTurnIndicator(false);

            showActionBtns(false);

            builtDistrictPnl.gameObject.SetActive(false);

            opponentGos[0].transform.parent.gameObject.SetActive(false);

            activityLogTxt.text = "";
        }

        public void setPlayerName(string name) {
            playerNameTxt.text = name;
        }

        public void updateCrown(int crownHolderI) {
            Text nameTxt;
            bool isCrownHolder;
            for(int i = 0; i < gMaster.playerList.Count; i++) {
                isCrownHolder = crownHolderI == i ? true : false;

                //Process player index to match UI index
                if(i == playerCon.playerListI) {
                    nameTxt = playerNameTxt;
                } else {
                    int panelIndex = i > playerCon.playerListI ? i - 1 : i;

                    if(gMaster.playerList.Count <= 3)
                        panelIndex += CData.MAX_OPPONENT_COUNT;

                    Transform opponentPanel = opponentGos[panelIndex].transform;
                    nameTxt = opponentPanel.GetChild((int)CData.OpponentPanel.Name).GetComponent<Text>();
                }

                //Set crown indicator by changing name color
                if(isCrownHolder) {
                    nameTxt.color = crownClr;
                    sfxSource.PlayOneShot(kingFanfare);
                } else {
                    nameTxt.color = Color.white;
                }
            }
        }

        public void updatePlayerGold() {
            goldTxt.text = playerCon.gold.ToString();
        }

        public void updatePlayerCharacter() {
            //If character's turn is done is or is current then reveal the character else hide it
            Sprite character = playerCon.characterFirst != (int)Character.Name.None ? CharacterIcons[playerCon.characterFirst] : null;
            setCharSprite(characterFirstImg, character);

            //Display the second character if player count <= 3 which uses 2 characters
            character = playerCon.characterSecond != (int)Character.Name.None ? CharacterIcons[playerCon.characterSecond] : null;
            setCharSprite(characterSecondImg, character);
        }

        //Sync player's built district with display objects
        public void updateBuiltDistricts() {
            for(int i = 0; i < builtDistrictPnl.childCount; i++) {
                Transform district = builtDistrictPnl.GetChild(i);

                if(i < playerCon.builtDistricts.Count) {
                    district.GetComponent<Image>().sprite = DistrictCards[playerCon.builtDistricts[i]];
                    district.gameObject.SetActive(true);
                } else {
                    district.gameObject.SetActive(false);
                }
            }
        }

        //Display the actions done for the whole game
        public void displayActivity() {
            //Flash log to indicate new activity
            logImg.CrossFadeAlpha(2, 0.001f, false);
            logImg.CrossFadeAlpha(logClr.a, logFlashDur, false);

            if(isLogPressed)
                return;

            activityLogTxt.text = gMaster.activityLog.ToString();
            StartCoroutine(cAutoScrollLog(1));
        }

        bool isLogPressed;

        public void onPressLogDown() {
            isLogPressed = true;
        }

        public void onPressLogUp() {
            isLogPressed = false;
            displayActivity();
        }

        IEnumerator cAutoScrollLog(float duration) {
            while(duration > 0) {
                yield return new WaitForEndOfFrame();

                if(isLogPressed)
                    duration = 0;

                activityLogScroll.value = Mathf.Lerp(activityLogScroll.value, 0, Time.deltaTime * scrollSpeed);
                duration -= Time.deltaTime;
            }
        }

        public void showActivityLog(bool isShow) {
            activityLogShowPnl.SetActive(isShow);
            if(isShow) {
                RectTransform canvas = activityLogRect.parent.GetComponent<RectTransform>();
                activityLogRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, canvas.rect.height);
                activityLogRect.SetAsLastSibling();
            } else {
                activityLogRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, orgActLogHeight);
                activityLogRect.SetAsFirstSibling();
            }
            StartCoroutine(cAutoScrollLog(1.5f));
        }

        //Activate panels based on number of opponents
        public void setupOpponentPanels() {
            opponentGos[0].transform.parent.gameObject.SetActive(true);
            foreach(GameObject panel in opponentGos)
                panel.SetActive(false);

            //Set Opponent Panel size to fit double hero or single based on player count
            GridLayoutGroup layout = opponentGos[0].GetComponentInParent<GridLayoutGroup>();
            layout.cellSize = PlayerController.playerCount > 3 ? singleCharPnlSize : doubleCharPnlSize;

            gMaster.playerList.Sort((player1, player2) => player1.playerListI.CompareTo(player2.playerListI));
            int panelIndex;
            for(int i = 0; i < gMaster.playerList.Count; i++) {
                //Don't create panel for player
                if(i == playerCon.playerListI)
                    continue;

                //Deduct offset since player is skipped
                panelIndex = i > playerCon.playerListI ? i - 1 : i;
                //Two and Three player panels are different, offset index
                if(gMaster.playerList.Count <= 3)
                    panelIndex += CData.MAX_OPPONENT_COUNT;

                //Initialize panel
                opponentGos[panelIndex].SetActive(true);
                Transform nameComp = opponentGos[panelIndex].transform.GetChild((int)CData.OpponentPanel.Name);
                nameComp.GetComponent<Text>().text = gMaster.playerList[i].playerName;
                updateOpponent(i);
            }

            playerCon.CmdSetupOpponentPanelsCallback();
        }

        //Update data displayed a single opponent
        public void updateOpponent(int i) {
            //Ignore if index belongs to player
            if(i == playerCon.playerListI)
                return;

            //Add offset since player is skipped
            int panelIndex = i > playerCon.playerListI ? i - 1 : i;
            //Two and Three player panels are different, offset index
            if(gMaster.playerList.Count <= 3)
                panelIndex += CData.MAX_OPPONENT_COUNT;

            GameObject opponentPanel = opponentGos[panelIndex];
            PlayerController opponent = gMaster.playerList[i];

            //Update opponent info displayed in player's UI
            Transform child = opponentPanel.transform.GetChild((int)CData.OpponentPanel.Gold);
            child.GetComponent<Text>().text = opponent.gold.ToString();

            child = opponentPanel.transform.GetChild((int)CData.OpponentPanel.Hand);
            child.GetComponent<Text>().text = opponent.hand.Count.ToString();

            child = opponentPanel.transform.GetChild((int)CData.OpponentPanel.District);
            child.GetComponent<Text>().text = opponent.builtDistricts.Count.ToString();

            //If opponent's turn is done is or is current then reveal the character else hide it, don't display if assassinated
            Image charFirstImg = opponentPanel.transform.GetChild((int)CData.OpponentPanel.Character1).GetComponent<Image>();
            //If no active character turn is displayed then do not show any characters
            if(turnIndicatorImg.gameObject.activeInHierarchy
                && opponent.characterFirst != (int)Character.Name.None
                && gMaster.curCharacterTurn >= opponent.characterFirst
                && gMaster.assassinatedChar != opponent.characterFirst) {
                setCharSprite(charFirstImg, CharacterIcons[opponent.characterFirst]);
            } else {
                setCharSprite(charFirstImg, null);
            }

            //Display the second character if player count <= 3 which uses 2 characters
            if(gMaster.playerList.Count > 3)
                return;
            Image charSecondImg = opponentPanel.transform.GetChild((int)CData.OpponentPanel.Character2).GetComponent<Image>();
            if(turnIndicatorImg.gameObject.activeInHierarchy
                && opponent.characterSecond != (int)Character.Name.None
                && gMaster.curCharacterTurn >= opponent.characterSecond
                && gMaster.assassinatedChar != opponent.characterSecond) {
                setCharSprite(charSecondImg, CharacterIcons[opponent.characterSecond]);
            } else {
                setCharSprite(charSecondImg, null);
            }
        }

        void setCharSprite(Image img, Sprite sprite) {
            img.sprite = sprite;
            img.color = sprite == null ? Color.clear : Color.white;
        }

        //Activate Character Select Panel
        public void initCharSelect(string description) {
            charSelDescTxt.text = description;


            charSelDisplay.sprite = cardBackSpr;
            playerCon.CmdSetSelectedChar((int)Character.Name.None);

            //Enable all characters, disable after based on purpose of selection
            foreach(Button item in charSelIcons) {
                item.interactable = true;
            }
            charSelPnl.SetActive(true);
        }

        //Disable/Enable character icons
        public void setupChar(int[] charList, bool isEnabled) {
            //Reset selection
            playerCon.CmdSetSelectedChar((int)Character.Name.None);
            charSelBtn.interactable = false;
            assassinateBtn.interactable = false;
            stealBtn.interactable = false;

            //Invert whole characters selection first
            foreach(Button item in charSelIcons) {
                item.image.color = (!isEnabled) ? Color.white : Color.gray;
            }

            //Enable/Disable the selected characters
            foreach(int character in charList) {
                if(character != (int)Character.Name.None)
                    charSelIcons[character].image.color = (isEnabled) ? Color.white : Color.gray;
            }
        }

        public void onPressCharacter(int charI) {
            //Save the index of selected character
            if(charSelIcons[charI].image.color == Color.white) {
                playerCon.CmdSetSelectedChar(charI);
                charSelBtn.interactable = true;
                assassinateBtn.interactable = true;
                stealBtn.interactable = true;
            } else {
                playerCon.CmdSetSelectedChar((int)Character.Name.None);
                charSelBtn.interactable = false;
                assassinateBtn.interactable = false;
                stealBtn.interactable = false;
            }

            //Display card
            charSelDisplay.sprite = CharacterCards[charI];
        }

        //End player input immediately on character select phase
        public void onPressSelectCharBtn() {
            charSelPnl.SetActive(false);
            playerCon.CmdWaitEnd();
        }

        public void onPressMagicianClose() {
            magicianPnl.SetActive(false);
            opponentPnl.SetParent(transform);
            opponentPnl.SetAsFirstSibling();

            isExchangeAbility = false;
        }

        //Hide character turn indicator
        public void displayTurnIndicator(bool isDisplay) {
            turnIndicatorImg.gameObject.SetActive(isDisplay);

            //Exit if turn display is hidden
            if(isDisplay)
                updateTurn(0);
        }

        //Display the info of current character taking turn
        public void updateTurn(int curTurnI) {
            Character.Name curChar = CData.characters[curTurnI];

            //Set the color of panel 
            Color charClr = neutralClr;
            if(curChar == Character.Name.King)
                charClr = nobleClr;
            else if(curChar == Character.Name.Bishop)
                charClr = religiousClr;
            else if(curChar == Character.Name.Merchant)
                charClr = tradeClr;
            else if(curChar == Character.Name.Warlord)
                charClr = militaryClr;

            turnIndicatorImg.color = charClr;
            turnIndicatorImg.CrossFadeAlpha(0, 0.001f, false);
            turnIndicatorImg.CrossFadeAlpha(0.8f, logFlashDur, false);

            turnNumTxt.text = (curTurnI + 1).ToString();

            turnCharTxt.text = CData.characters[curTurnI].ToString();

            PlayerController player = gMaster.playerCharTurns[curTurnI];
            if(player != null)
                turnPlayerTxt.text = player.playerName;
            else
                turnPlayerTxt.text = "";
        }

        SyncListInt displayedList;
        //Display player's hand
        public void showCardList(SyncListInt cardList, string description) {
            displayedList = cardList;

            handDescTxt.text = description;

            //Check if there is a difference between card list and display objects
            int cardDifference = handGrid.childCount - cardList.Count;

            if(cardDifference < 0) {
                //If display objects are less than the card list then create more
                for(int i = 0; i > cardDifference; i--) {
                    //Create new card display object and set its parent
                    GameObject go = Instantiate(cardPrefab);
                    RectTransform newCard = go.GetComponent<RectTransform>();
                    newCard.SetParent(handGrid);
                    newCard.sizeDelta = handGrid.GetComponent<GridLayoutGroup>().cellSize;
                }
            } else {
                //Sync card list with display objects
                for(int i = 0; i < handGrid.childCount; i++) {
                    bool isActive = i < cardList.Count ? true : false;
                    handGrid.GetChild(i).gameObject.SetActive(isActive);
                }
            }

            //Set card list info to display objects
            for(int i = 0; i < cardList.Count; i++) {
                Image display = handGrid.GetChild(i).GetComponent<Image>();
                display.sprite = DistrictCards[cardList[i]];

                //Set button to return its index on click to track selected card
                Button button = handGrid.GetChild(i).GetComponent<Button>();
                button.onClick.RemoveAllListeners();

                //Save parameter first to a temporary variable, passing it directly causes problems (same var/val is passed to all?)
                int cardIndex = i;
                button.onClick.AddListener(() => displayCard(cardIndex));

                button.interactable = true;
            }

            //Activate/show card viewer panel after all data are laid out
            handPanel.SetActive(true);
            handCardImg.gameObject.SetActive(false);
            buildSelectedBtn.gameObject.SetActive(false);
            exchangeSelectionBtn.gameObject.SetActive(false);
            exchangeSelectCardBtn.gameObject.SetActive(false);
        }

        void displayCard(int index) {
            //Track last pressed district card
            selectedDistrictI = index;
            handCardImg.gameObject.SetActive(true);
            handCardImg.sprite = DistrictCards[displayedList[index]];
        }

        public void onPressHandBtn() {
            //Ignore if hand panel is already open
            if(handPanel.activeSelf)
                return;
            showCardList(playerCon.hand, playerCon.playerName + "'s Hand");
        }

        //Let player select between gold or district card
        public void showActionSelect() {
            selectedDistrictI = 0;
            actionDescTxt.text = "Select an action";

            string dCount = (gMaster.districtDeck.Count > 0) ? gMaster.districtDeck.Count.ToString() : "Nothing";
            districtCountTxt.text = dCount + " left";
            Debug.Log("--- " + System.Reflection.MethodBase.GetCurrentMethod().Name + " ---");
            Debug.Log("  " + "districtCountTxt.text: " + districtCountTxt.text);

            actionPanel.SetActive(true);
            actionGoldBtn.SetActive(true);
            actionDistBtn.SetActive(true);
            actionDistPnl.gameObject.SetActive(false);
            cards = new District.Name[0];

            //Disable district button if no more district cards can be drawn
            actionDistBtn.GetComponent<Button>().interactable = (gMaster.districtDeck.Count > 0) ? true : false;
        }

        //Player takes gold from bank
        public void onActionGold() {
            playerCon.CmdAddGold(CData.ACTION_GOLD_GAIN);

            updatePlayerGold();

            actionPanel.SetActive(false);

            playerCon.CmdWaitEnd();
            playerCon.CmdLog(playerCon.playerName + " took gold");
        }

        //Show district cards to select from
        public void onActionDistrict() {
            actionDescTxt.text = "Select a district";
            actionDistPnl.gameObject.SetActive(true);
            actionGoldBtn.SetActive(false);
            actionDistBtn.SetActive(false);

            districtDrawCount = CData.ACTION_DISTRICT_CHOICE;
            districtHandCount = CData.ACTION_DISTRICT_HAND;

            //Choose from 3 district cards if Observatory is built
            if(playerCon.builtDistricts.Contains((int)District.Name.Observatory))
                districtDrawCount++;

            //Keep 2 districts instead if Library is built
            if(playerCon.builtDistricts.Contains((int)District.Name.Library))
                districtHandCount++;

            cards = gMaster.drawDistricts(districtDrawCount);

            for(int i = 0; i < districtDrawCount; i++) {
                actionDistPnl.GetChild(i).gameObject.SetActive(true);
                Image district = actionDistPnl.GetChild(i).GetComponent<Image>();
                district.sprite = DistrictCards[(int)cards[i]];
            }
        }

        //Track selected district
        public void onSelectDistrict(int selectedCard) {
            actionDistPnl.GetChild(selectedCard).gameObject.SetActive(false);

            playerCon.CmdAddDistrict((int)cards[selectedCard]);

            //Pick another district to keep if under the bonus of Library
            districtHandCount--;
            if(districtHandCount > 0)
                return;

            //Return extra cards back to district deck
            for(int i = 0; i < CData.ACTION_DISTRICT_CHOICE; i++) {
                //Do not return selected card
                if(i == selectedCard)
                    continue;

                playerCon.CmdReturnDistDeck((int)cards[i]);
            }

            //Reset card references
            cards = new District.Name[0];

            //Close action panels
            for(int i = 0; i < districtDrawCount; i++) {
                actionDistPnl.GetChild(i).gameObject.SetActive(false);
            }
            actionDistPnl.gameObject.SetActive(false);
            actionPanel.SetActive(false);
            playerCon.CmdWaitEnd();
            playerCon.CmdLog(playerCon.playerName + " took district");
        }

        //Activate/deactivate buttons to control player actions
        public void showActionBtns(bool isShow) {
            if(isShow) {
                if(playerCon.builtDistricts.Contains((int)District.Name.Laboratory) && !playerCon.isLaboratoryUsed) {
                    laboratoryBtn.gameObject.SetActive(true);
                    laboratoryBtn.interactable = true;
                }
                if(playerCon.builtDistricts.Contains((int)District.Name.Smithy) && !playerCon.isSmithyUsed) {
                    smithyBtn.gameObject.SetActive(true);
                    //Disable smithy button if there are no more cards in district deck
                    smithyBtn.interactable = (gMaster.districtDeck.Count > 0) ? true : false;
                }

                //Hide ability button when character is Architect since there's no active ability
                if((int)Character.Name.Architect == gMaster.curCharacterTurn)
                    abilityBtn.gameObject.SetActive(false);
                else
                    abilityBtn.gameObject.SetActive(true);
                //On enable player actions, allow usage of character abilty
                abilityBtn.interactable = (!playerCon.isAbilityUsed) ? true : false;
                //On enable player actions, allow building districts
                buildBtn.interactable = (playerCon.buildCount > 0) ? true : false;
            } else {
                laboratoryBtn.gameObject.SetActive(false);
                smithyBtn.gameObject.SetActive(false);
                abilityBtn.gameObject.SetActive(false);
            }

            buildBtn.gameObject.SetActive(isShow);
            endBtn.gameObject.SetActive(isShow);
        }

        //Execute the ability of current character
        public void onPressAbility() {
            //Show tutorial for new players
            if(CData.playerProfile.isTutorial)
                StartCoroutine(cShowTutorial(CData.TUTORIAL_CHAR_ABILITY));

            switch(gMaster.curCharacterTurn) {
                case (int)Character.Name.Assassin:
                    setupAssassinate();
                    break;
                case (int)Character.Name.Thief:
                    setupSteal();
                    break;
                case (int)Character.Name.Magician:
                    abilityMagician();
                    break;
                case (int)Character.Name.King:
                    setupTaxPnl(District.Type.Noble);
                    break;
                case (int)Character.Name.Bishop:
                    setupTaxPnl(District.Type.Religious);
                    break;
                case (int)Character.Name.Merchant:
                    setupTaxPnl(District.Type.Trade);
                    break;
                case (int)Character.Name.Warlord:
                    setupTaxPnl(District.Type.Military);
                    break;
                default:
                    break;
            }
        }

        //Display district card hand then select one to build
        public void onPressBuild() {
            showCardList(playerCon.hand, "Build a district");

            //Add button click event to check if player can build selected district
            for(int i = 0; i < playerCon.hand.Count; i++) {
                Button button = handGrid.GetChild(i).GetComponent<Button>();

                if(playerCon.builtDistricts.Contains(playerCon.hand[i]))
                    button.interactable = false;

                //Save parameter first to a temporary variable, passing it directly causes problems (same var/val is passed to all?)
                int cardIndex = i;
                button.onClick.AddListener(() => buildCheck(cardIndex));
            }

            //Show tutorial for new players
            if(CData.playerProfile.isTutorial)
                StartCoroutine(cShowTutorial(CData.TUTORIAL_DIST_BUILD));
        }

        void buildCheck(int index) {
            int buildCost = CData.getDistrictData(District.Data.Cost, CData.districtNames[playerCon.hand[index]]);

            bool canBuild = playerCon.gold >= buildCost && playerCon.builtDistricts.Count < CData.BUILD_COUNT_LIMIT;

            buildSelectedBtn.interactable = canBuild;
            buildSelectedBtn.gameObject.SetActive(true);
        }

        public void buildSelected() {
            District.Name district = CData.districtNames[playerCon.hand[selectedDistrictI]];

            //Deduct build cost from player's gold
            int buildCost = CData.getDistrictData(District.Data.Cost, CData.districtNames[playerCon.hand[selectedDistrictI]]);
            playerCon.CmdAddGold(-buildCost);

            updatePlayerGold();

            //Transfer from hand to built districts
            playerCon.CmdBuildDistrict(playerCon.hand[selectedDistrictI]);

            updateBuiltDistricts();

            buildSelectedBtn.gameObject.SetActive(false);
            handPanel.SetActive(false);

            //Reduce player's allowable build count
            playerCon.buildCount--;
            buildBtn.interactable = (playerCon.buildCount > 0) ? true : false;

            if(district == District.Name.Smithy)
                smithyBtn.gameObject.SetActive(true);

            if(district == District.Name.Laboratory)
                laboratoryBtn.gameObject.SetActive(true);
        }

        //End player's turn by turning wait input flag off
        public void onPressEnd() {
            //Show tutorial for new players
            if(CData.playerProfile.isTutorial)
                StartCoroutine(cShowTutorial(CData.TUTORIAL_LOOP));

            isExchangeAbility = false;
            isDestroyAbility = false;

            playerCon.CmdWaitEnd();
        }

        //Display player's built districts
        public void onPressBuiltDist() {
            showCardList(playerCon.builtDistricts, playerCon.playerName + "'s city");
        }

        public void onPressOpponent(int oppIndex) {
            //Add offset since player is skipped
            selectedOpponentI = oppIndex >= playerCon.playerListI ? oppIndex + 1 : oppIndex;

            //Exchange hand just needs opponent index
            if(isExchangeAbility) {
                playerCon.CmdExchangePlayer(selectedOpponentI);
                updateOpponent(selectedOpponentI);
                abilityBtn.interactable = false;
                onPressMagicianClose();
                return;
            }

            PlayerController opponent = gMaster.playerList[selectedOpponentI];
            showCardList(opponent.builtDistricts, opponent.playerName + "'s city");

            //Add listener to buttons to check destroy function
            if(isDestroyAbility) {
                for(int i = 0; i < opponent.builtDistricts.Count; i++) {
                    Button button = handGrid.GetChild(i).GetComponent<Button>();

                    //Save parameter first to a temporary variable, passing it directly causes problems (same var/val is passed to all?)
                    int cardIndex = i;
                    button.onClick.AddListener(() => destroyCheck(cardIndex));
                }
            }
        }

        //Display the whole character card when character icon is clicked
        public void onPressCharFirstBtn() {
            showCard(playerCon.characterFirst);
        }

        public void onPressCharSecondBtn() {
            showCard(playerCon.characterSecond);
        }

        //Display the whole character card depending on current turn's character
        public void onPressTurnBtn() {
            showCard(gMaster.curCharacterTurn);
        }

        //Display the whole character card
        public void showCard(int character) {
            if(character == (int)Character.Name.None)
                return;

            singleCardPnl.SetActive(true);
            Image cardImage = singleCardPnl.transform.GetChild(0).GetComponent<Image>();
            cardImage.sprite = CharacterCards[character];
        }

        //Choose a character, skip their turn
        public void setupAssassinate() {
            initCharSelect("Assassinate a character");

            //Assassin cannot kill himself
            int[] exception = { (int)Character.Name.Assassin,
                playerCon.characterFirst,
                playerCon.characterSecond
            };

            setupChar(exception, false);
            assassinateBtn.gameObject.SetActive(true);
        }

        public void onPressAssassinate() {
            //Mark the assassinated character
            playerCon.CmdAssassinate();

            assassinateBtn.gameObject.SetActive(false);
            abilityBtn.interactable = false;
            charSelPnl.SetActive(false);
        }

        //Choose a character, steal all their turn on start of their turn
        public void setupSteal() {
            initCharSelect("Steal from a character");

            int[] exception = {
                (int)Character.Name.Assassin, (int)Character.Name.Thief,
                playerCon.characterFirst, playerCon.characterSecond,
                gMaster.assassinatedChar
            };

            setupChar(exception, false);
            stealBtn.gameObject.SetActive(true);
        }

        public void onPressSteal() {
            //Mark the character to steal from
            playerCon.CmdSteal();

            stealBtn.gameObject.SetActive(false);
            abilityBtn.interactable = false;
            charSelPnl.SetActive(false);
        }

        //Show magician ability selection
        public void abilityMagician() {
            magicianDescTxt.text = "Select an action";
            magicianPnl.SetActive(true);

            exchangePlayerBtn.SetActive(true);
            exchangeDeckBtn.SetActive(true);
            //Disable exchange with deck if deck is empty already
            exchangeDeckBtn.GetComponent<Button>().interactable = (gMaster.districtDeck.Count > 0) ? true : false;

            isExchangeAbility = false;
        }

        //Display opponent selection
        public void onPressExchangePlayer() {
            magicianDescTxt.text = "Select an opponent to exchange hand with";
            exchangePlayerBtn.SetActive(false);
            exchangeDeckBtn.SetActive(false);

            opponentPnl.SetParent(magicianPnl.transform);
            opponentPnl.SetAsLastSibling();

            isExchangeAbility = true;
        }

        //Display player's hand for selection
        public void onPressExchangeDeckBtn() {
            showCardList(playerCon.hand, "Select districts to exchange with main district deck");

            magicianPnl.SetActive(false);

            exchangeList = new List<int>();
            exchangeSelectionBtn.gameObject.SetActive(true);
            exchangeSelectionBtn.interactable = false;
            exchangeSelectCardBtn.gameObject.SetActive(true);
            exchangeSelectCardBtn.interactable = false;

            //Add button click event to set select card button
            for(int i = 0; i < playerCon.hand.Count; i++) {
                Button button = handGrid.GetChild(i).GetComponent<Button>();

                button.onClick.AddListener(() => {
                    exchangeSelectCardBtn.interactable = true;
                });
            }
        }

        //Get currently selected district and add to exchange list
        public void onPressExchangeSelectCardBtn() {
            exchangeList.Add(selectedDistrictI);
            handGrid.GetChild(selectedDistrictI).gameObject.SetActive(false);

            exchangeSelectCardBtn.interactable = false;
            exchangeSelectionBtn.interactable = true;
            handCardImg.gameObject.SetActive(false);
        }

        //Commit exchange from cards in the exchange list
        public void onPressExchangeSelectionBtn() {
            playerCon.CmdExchangeDeck(exchangeList.ToArray());

            handPanel.SetActive(false);
            abilityBtn.interactable = false;
        }

        void setupTaxPnl(District.Type district) {
            int taxAmount = playerCon.getTaxAmount(district);
            string question = "Take " + taxAmount + " gold from taxing " + district + " districts?";
            Sprite display;
            switch(district) {
                case District.Type.Military:
                    display = DistrictCards[(int)District.Name.Fortress];
                    break;
                case District.Type.Noble:
                    display = DistrictCards[(int)District.Name.Palace];
                    break;
                case District.Type.Trade:
                    display = DistrictCards[(int)District.Name.TownHall];
                    break;
                case District.Type.Religious:
                    display = DistrictCards[(int)District.Name.Cathedral];
                    break;
                default:
                    display = DistrictCards[(int)District.Name.DragonGate];
                    break;
            }
            showChoice(question, display);
            choiceYesBtn.onClick.RemoveAllListeners();
            choiceYesBtn.onClick.AddListener(() => {
                playerCon.CmdTaxDistricts((int)district, taxAmount);
            });
        }

        //Initialize and display destroy interface
        public void showDestroyPnl() {
            destroyPnl.SetActive(true);
            destroyBtn.gameObject.SetActive(false);

            disableOppPanel(gMaster.playerCharTurns[(int)Character.Name.Bishop]);
            foreach(var player in gMaster.playerList) {
                if(player != playerCon && player.builtDistricts.Count >= CData.BUILD_COUNT_LIMIT)
                    disableOppPanel(player);
            }

            opponentPnl.SetParent(destroyPnl.transform);
            opponentPnl.SetAsLastSibling();

            isDestroyAbility = true;
        }

        void disableOppPanel(PlayerController player) {
            if(player != null && player != playerCon) {
                //Deduct offset since player is skipped
                int panelIndex = player.playerListI > playerCon.playerListI ? player.playerListI - 1 : player.playerListI;
                //Two and Three player panels are different, offset index
                if(gMaster.playerList.Count <= 3)
                    panelIndex += CData.MAX_OPPONENT_COUNT;

                opponentBtn = opponentGos[panelIndex].GetComponent<Button>();
                opponentBtn.interactable = false;
            }
        }

        int destroyCost;
        //Checks selected district if player can afford to destroy it
        public void destroyCheck(int districtI) {
            PlayerController opponent = gMaster.playerList[selectedOpponentI];
            destroyBtn.gameObject.SetActive(true);

            //Keep cannot be destroyed
            if(opponent.builtDistricts[districtI] == (int)District.Name.Keep) {
                destroyBtn.interactable = false;
                return;
            }

            int targetDistCost = CData.getDistrictData(District.Data.Cost, CData.districtNames[opponent.builtDistricts[districtI]]) - 1;

            //Negate cost deduction if opponent has Great Wall
            if(opponent.builtDistricts.Contains((int)District.Name.GreatWall) &&
                        opponent.builtDistricts[districtI] != (int)District.Name.GreatWall) {
                targetDistCost++;
            }

            destroyCost = targetDistCost;
            destroyBtn.interactable = (targetDistCost <= playerCon.gold) ? true : false;
        }

        //Commits destroy ability
        public void onPressDestroyBtn() {
            playerCon.CmdDestroyDistrict(selectedOpponentI, selectedDistrictI, destroyCost);

            handPanel.SetActive(false);
            onPressDestroyClose();
        }

        //Clean up after destroy process
        public void onPressDestroyClose() {
            isDestroyAbility = false;

            //If no Bishop was encountered, could be null
            if(opponentBtn != null)
                opponentBtn.interactable = true;

            opponentPnl.SetParent(transform);
            opponentPnl.SetAsFirstSibling();

            destroyBtn.gameObject.SetActive(false);
            destroyPnl.SetActive(false);
            playerCon.CmdWaitEnd();
        }

        public void showChoice(string description, Sprite card = null) {
            choicePnl.SetActive(true);
            choiceYesBtn.gameObject.SetActive(true);
            choiceDescTxt.text = description;

            if(card != null) {
                choiceDisplayImg.gameObject.SetActive(true);
                choiceDisplayImg.sprite = card;
            } else {
                choiceDisplayImg.gameObject.SetActive(false);
            }
        }

        public void onPressChoice(bool choice) {
            choicePnl.SetActive(false);
        }

        public void onPressSmithy() {
            showChoice("Pay two gold to draw 3 district cards?", DistrictCards[(int)District.Name.Smithy]);
            if(playerCon.gold >= CData.SMITHY_COST) {
                choiceYesBtn.interactable = true;
                choiceYesBtn.onClick.RemoveAllListeners();
                choiceYesBtn.onClick.AddListener(effectSmithy);
            } else {
                choiceYesBtn.interactable = false;
            }
        }

        void effectSmithy() {
            if(gMaster.districtDeck.Count <= 0)
                return;
            playerCon.CmdSmithy();
            playerCon.isSmithyUsed = true;
            sfxSource.PlayOneShot(districtCoins);
            smithyBtn.interactable = false;
        }

        public void onPressLaboratory() {
            tradeGoldBtn.SetActive(false);
            showCardList(playerCon.hand, "Trade a district for a gold");

            for(int i = 0; i < playerCon.hand.Count; i++) {
                Button button = handGrid.GetChild(i).GetComponent<Button>();

                //Save parameter first to a temporary variable, passing it directly causes problems (same var/val is passed to all?)
                button.onClick.AddListener(() => {
                    tradeGoldBtn.SetActive(true);
                });
            }
        }

        public void onPressTradeGold() {
            playerCon.CmdLaboratory(playerCon.hand[selectedDistrictI]);

            tradeGoldBtn.SetActive(false);
            handPanel.SetActive(false);
            laboratoryBtn.interactable = false;
            sfxSource.PlayOneShot(districtCoins);
            playerCon.isLaboratoryUsed = true;
        }

        public void onPressMainMenu() {
            StartCoroutine(mainMenuTransition());
        }

        //Quit the game and return to main menu
        IEnumerator mainMenuTransition() {
            activityLogShowPnl.SetActive(false);
            optionPnl.SetActive(false);

            float duration = 3;
            activityLogShowPnl.GetComponent<Image>().CrossFadeAlpha(0, 0.001f, true);
            activityLogShowPnl.GetComponent<Button>().interactable = false;
            activityLogShowPnl.SetActive(true);
            activityLogShowPnl.GetComponent<Image>().CrossFadeAlpha(2, duration, true);
            yield return new WaitForSeconds(duration);

            //Use Lobby Manager back function
            Prototype.NetworkLobby.LobbyManager.s_Singleton.GoBackButton();
        }

        public IEnumerator cShowTutorial(int index) {
            tutorialPnl.SetActive(true);

            foreach(var item in tutorialTxts) {
                item.SetActive(false);
            }

            tutorialTxts[index].SetActive(true);

            yield return new WaitForEndOfFrame();
            //Pause game when displaying tutorial
            Time.timeScale = 0;
        }

        public void onPressTutorial() {
            Time.timeScale = 1;
            tutorialPnl.SetActive(false);
        }

        public void onBgmVolChange() {
            CData.playerProfile.bgmVol = bgmVolSlider.value;
        }

        public void onSfxVolChange() {
            CData.playerProfile.sfxVol = sfxVolSlider.value;
        }
    }
}
