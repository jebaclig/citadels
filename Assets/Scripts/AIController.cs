using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Citadel
{
    public class AIController : PlayerController_Single
    {
        [Header("AI Preferences")]
        [Range(1, 6)]
        public int prefDistCost = 4;
        //Used to weigh priority of sticking close to preferred district cost
        [Range(0.1f, 0.2f)]
        public float distCostPrio = 0.15f;
        //Modify cost priority to weigh taking gold or district
        public const float distCostPrioMod = 2.5f;
        //If a district's score in hand exceeds this, it will not be built
        public float buildThreshold = 0.15f;

        void Awake() {
            setPlayerName();
        }

        //Get a random name
        protected new void setPlayerName() {
            int nameI = Random.Range(0, CData.AI_NAMES.Length - CData.nameGetCount);
            playerName = CData.AI_NAMES[nameI];

            CData.nameGetCount++;
            CData.AI_NAMES[nameI] = CData.AI_NAMES[CData.AI_NAMES.Length - CData.nameGetCount];
        }

        //Select character based on current situation
        int selectCharacter() {
            calculateCardTypes();

            if(gMaster.characterDeck.Contains(Character.Name.Warlord) && cardPercentages[District.Type.Military] > Random.value)
                return gMaster.characterDeck.FindIndex(character => character == Character.Name.Warlord);

            if(gMaster.characterDeck.Contains(Character.Name.King) && cardPercentages[District.Type.Noble] > Random.value)
                return gMaster.characterDeck.FindIndex(character => character == Character.Name.King);

            if(gMaster.characterDeck.Contains(Character.Name.Merchant) && cardPercentages[District.Type.Trade] > Random.value)
                return gMaster.characterDeck.FindIndex(character => character == Character.Name.Merchant);

            if(gMaster.characterDeck.Contains(Character.Name.Bishop) && cardPercentages[District.Type.Religious] > Random.value)
                return gMaster.characterDeck.FindIndex(character => character == Character.Name.Bishop);

            if(gMaster.characterDeck.Contains(Character.Name.Architect) &&
                (gold >= CData.BUILD_COST_MAX || hand.Count <= CData.HAND_START_COUNT))
                return gMaster.characterDeck.FindIndex(character => character == Character.Name.Architect);

            if(gMaster.characterDeck.Contains(Character.Name.Magician) && decideSelectMagician())
                return gMaster.characterDeck.FindIndex(character => character == Character.Name.Magician);

            if(gMaster.characterDeck.Contains(Character.Name.Thief))
                return gMaster.characterDeck.FindIndex(character => character == Character.Name.Thief);

            if(gMaster.characterDeck.Contains(Character.Name.Assassin))
                return gMaster.characterDeck.FindIndex(character => character == Character.Name.Assassin);

            return Random.Range(0, gMaster.characterDeck.Count);
        }

        public override IEnumerator cCharSelectPhase() {
            goldGainRound = 0;
            builtDistrictRound = 0;

            yield return new WaitForSeconds(Random.Range(0, gMaster.transitionPauseDur));

            int selection;
            //Two-Player Rule, need to discard when selecting a character
            if(gMaster.playerList.Count == 2) {
                selection = Random.Range(0, gMaster.characterDeck.Count);
                //The first player to select character doesn't discard
                if(gMaster.characterDeck.Count < CData.CHARACTER_COUNT - 1) {
                    gMaster.characterDiscard.Add(gMaster.characterDeck[selection]);
                    gMaster.characterDeck.RemoveAt(selection);
                }
            }

            //Seven Player Rule, the last player can select between last character and first discarded character
            if(gMaster.characterDeck.Count == 1 && gMaster.playerList.Count == 7) {
                gMaster.characterDeck.Add(gMaster.characterDiscard[0]);
                gMaster.characterDiscard.RemoveAt(0);
            }

            //Select a character from the available choices
            selection = selectCharacter();

            if(characterFirst == Character.Name.None) {
                characterFirst = gMaster.characterDeck[selection];
                gMaster.characterDeck.RemoveAt(selection);
            }
            //Two and Three-player Rule, all players use 2 Characters
            else if(gMaster.playerList.Count <= 3 && characterFirst != Character.Name.None) {
                characterSecond = gMaster.characterDeck[selection];
                gMaster.characterDeck.RemoveAt(selection);

                //Switch character slots if 2nd character goes first
                if((int)characterFirst > (int)characterSecond) {
                    Character.Name temp = characterSecond;
                    characterSecond = characterFirst;
                    characterFirst = temp;
                }
            }
        }

        public override IEnumerator cDistrictPhase() {
            onStartTurn();
            uiManager.updateOpponent(playerListI);

            yield return new WaitForSeconds(Random.Range(0, gMaster.transitionPauseDur));

            //Take an action
            if(isTakeGold() || gMaster.districtDeck.Count <= 0)
                takeGold();
            else
                takeDistrict();

            yield return new WaitForSeconds(Random.Range(0, gMaster.transitionPauseDur));

            //Use character ability
            useAbility();

            if(builtDistricts.Contains(District.Name.Smithy) && gold > CData.SMITHY_COST && hand.Count <= CData.HAND_START_COUNT)
                smithy();

            //Build district
            selectDistrict();

            yield return new WaitForSeconds(Random.Range(0, gMaster.transitionPauseDur));

            if((int)Character.Name.Warlord == gMaster.curCharacterTurn)
                destroyDistrict();

            uiManager.log(playerName + " End Turn \n");
        }

        //Decide if AI will take gold or district
        bool isTakeGold() {
            //Random value to serve as basis if AI will get gold
            float choiceRoll = Random.value;

            //Get average cost of districts at hand to serve as basis for cost preference, used for scoring
            float averageCost = 0;
            for(int i = 0; i < hand.Count; i++)
                averageCost += CData.getDistrictData(District.Data.Cost, hand[i]);
            averageCost /= hand.Count;
            prefDistCost = (int)averageCost;

            //If hand is empty, take district instead
            float goldChance = 0;
            if(hand.Count > 0) {
                District.Name[] arrHand = hand.ToArray();
                float[] handScores = getScores(arrHand);

                //Sort districts at hand based on scores given
                System.Array.Sort(handScores, arrHand);

                //Get the cost of the most ideal district at hand to serve as preference
                int bestDistCost = CData.getDistrictData(District.Data.Cost, arrHand[arrHand.Length - 1]);
                prefDistCost = bestDistCost;

                //The lesser the gold from the cost of the most ideal district at hand, the more likely to take
                goldChance = (gold - bestDistCost) * (distCostPrio * distCostPrioMod);

                //If gold is low, increase chances of taking more
                goldChance -= (gold <= CData.GOLD_START_COUNT) ? distCostPrio * distCostPrioMod : 0;

                //Roll again if current gold is enough to build desired district
                if(goldChance >= 0 && arrHand.Length > 1) {
                    int deductedGold = gold - bestDistCost;
                    bestDistCost = CData.getDistrictData(District.Data.Cost, arrHand[arrHand.Length - 2]);
                    goldChance = (deductedGold - bestDistCost) * (distCostPrio * distCostPrioMod);
                }
            }
            //Check calculated chance with a random value to decide if gold should be taken
            return (hand.Count > 0 && choiceRoll + goldChance <= 0) ? true : false;
        }



        void selectDistrict() {
            District.Name[] arrHand = hand.ToArray();

            //Use scoring to select a district to build
            float[] handScores = getScores(arrHand);

            //Sort hand based on scores
            System.Array.Sort(handScores, arrHand);

            //Set a minimum score to avoid forcing build every turn
            if(handScores[handScores.Length - 1] >= buildThreshold) {
                hand = new List<District.Name>(arrHand);
                for(int i = handScores.Length - 1; i >= 0; i--) {
                    //Check if score is above minimum, else don't build
                    if(handScores[i] >= buildThreshold) {
                        if(gold >= CData.getDistrictData(District.Data.Cost, hand[i]) && !builtDistricts.Contains(hand[i])) {
                            buildDistrict(i);

                            //Stop building districts 
                            if(buildCount <= 0) break;
                        }
                    } else if(i == 0 && builtDistricts.Contains(District.Name.Laboratory)) {
                        laboratory(hand[i]);
                    }
                }
            }
        }

        //Transfer district from hand to built, reduce allowable build count
        void buildDistrict(int handI) {
            gold -= CData.getDistrictData(District.Data.Cost, hand[handI]);
            builtDistricts.Add(hand[handI]);
            uiManager.log(playerName + " built " + hand[handI]);

            if(CData.getCardType(hand[handI]) == District.Type.Special)
                uiManager.addSummary(playerName + " built a special district, the " + hand[handI]);

            hand.RemoveAt(handI);
            uiManager.updateOpponent(playerListI);
            buildCount--;
            builtDistrictRound++;
            uiManager.sfxSource.PlayOneShot(uiManager.opponentBuild);
        }

        void takeGold() {
            gold += CData.ACTION_GOLD_GAIN;
            goldGainRound += CData.ACTION_GOLD_GAIN;

            uiManager.sfxSource.PlayOneShot(uiManager.opponentGold);
            uiManager.log(playerName + " took gold");
            uiManager.updateOpponent(playerListI);
        }

        void takeDistrict() {
            if(gMaster.districtDeck.Count <= 0)
                return;

            //Take district
            int drawCount = CData.ACTION_DISTRICT_CHOICE;
            if(builtDistricts.Contains(District.Name.Observatory))
                drawCount++;
            District.Name[] drawCards = gMaster.drawDistricts(drawCount);

            //Give cards score
            float[] drawScores = getScores(drawCards);

            //Pick the card with the highest score
            System.Array.Sort(drawScores, drawCards);
            hand.Add(drawCards[drawCards.Length - 1]);

            if(builtDistricts.Contains(District.Name.Library))
                hand.Add(drawCards[drawCards.Length - 2]);

            //Return other card to district deck
            for(int i = 0; i < drawCards.Length - 1; i++)
                gMaster.returnDistrict(drawCards[i]);
            drawCards = new District.Name[0];

            uiManager.log(playerName + " took district");
            uiManager.updateOpponent(playerListI);
        }

        //Check for dominant card color to act as preferred type to build
        Dictionary<District.Type, float> cardPercentages;
        void calculateCardTypes() {
            //Initialize card count
            cardPercentages = new Dictionary<District.Type, float>();
            foreach(District.Type cartType in CData.districtTypes)
                cardPercentages.Add(cartType, 0);

            //Total district cards in player's possession
            int totalCards = hand.Count + builtDistricts.Count;

            //Iterate through player's hand and built districts and count by types
            List<District.Name> tempList = hand;
            int index = 0;
            for(int i = 0; i < totalCards; i++) {
                //Determine if hand or built district
                index = i;
                if(i >= hand.Count) {
                    tempList = builtDistricts;
                    //No need to convert index if hand is empty
                    if(hand.Count > 0)
                        index = i % hand.Count;
                }

                //Check the type of district and increment
                cardPercentages[CData.getCardType(tempList[index])]++;
            }

            //Convert count to percentage
            foreach(District.Type item in CData.districtTypes) {
                cardPercentages[item] /= totalCards;
            }
        }

        //Give cards score
        float[] getScores(District.Name[] cards) {
            //Analyze current cards
            calculateCardTypes();

            float[] scores = new float[cards.Length];

            for(int i = 0; i < cards.Length; i++) {
                if(builtDistricts.Contains(cards[i])) {
                    scores[i] = -1;
                    continue;
                }

                //Give base score for how much its type matches the rest of the cards
                scores[i] = cardPercentages[CData.getCardType(cards[i])];

                //Penalize score based on how far the card's cost from the preferred cost of the player
                int costDif = Mathf.Abs(prefDistCost - CData.getDistrictData(District.Data.Cost, cards[i]));
                scores[i] -= costDif * distCostPrio;
            }
            return scores;
        }

        public void useAbility() {
            switch(gMaster.curCharacterTurn) {
                case (int)Character.Name.Assassin:
                    assassinate();
                    break;
                case (int)Character.Name.Thief:
                    steal();
                    break;
                case (int)Character.Name.Magician:
                    decideExchange();
                    break;
                case (int)Character.Name.King:
                    taxDistricts(District.Type.Noble);
                    break;
                case (int)Character.Name.Bishop:
                    taxDistricts(District.Type.Religious);
                    break;
                case (int)Character.Name.Merchant:
                    taxDistricts(District.Type.Trade);
                    break;
                case (int)Character.Name.Warlord:
                    taxDistricts(District.Type.Military);
                    break;
                default:
                    break;
            }
        }

        enum Criteria
        {
            District, Gold, Hand
        }

        PlayerController_Single[] sortTopTargets(Criteria criteria) {
            //Give a score depending on how close they are to winning
            PlayerController_Single[] players = gMaster.playerList.ToArray();
            int[] scores = new int[players.Length];

            for(int i = 0; i < players.Length; i++) {
                if(i == playerListI) {
                    scores[i] = -1;
                    continue;
                }

                switch(criteria) {
                    case Criteria.District:
                        scores[i] = players[i].cityScore();
                        break;
                    case Criteria.Gold:
                        scores[i] = players[i].gold;
                        break;
                    case Criteria.Hand:
                        scores[i] = players[i].hand.Count;
                        break;
                    default:
                        break;
                }
            }
            System.Array.Sort(scores, players);

            return players;
        }

        Character.Name selectTarget(Character.Name[] excemption, Criteria criteria) {
            //Create a list of characters and remove invalid targets
            List<Character.Name> characters = new List<Character.Name>(CData.characters);
            foreach(var item in excemption) {
                characters.Remove(item);
            }

            //Random to a valid character to steal first
            int charIndex = Random.Range(0, characters.Count);
            Character.Name selectedChar = characters[charIndex];
            List<PlayerController_Single> players = new List<PlayerController_Single>(sortTopTargets(criteria));

            //Iterate through players to check who's nearest to completion
            for(int i = players.Count - 1; i >= 1; i--) {
                if((i / (float)players.Count) > Random.value) {
                    //Check if selected player holds valid character to assassinate
                    if(characters.Contains(players[i].characterSecond) && players.Count <= 3 && Random.value > 0.5f)
                        selectedChar = players[i].characterSecond;
                    else if(characters.Contains(players[i].characterFirst))
                        selectedChar = players[i].characterFirst;
                    break;
                }
            }
            return selectedChar;
        }

        void assassinate() {
            Character.Name[] invalidChars = {
            Character.Name.None, Character.Name.Assassin, characterFirst, characterSecond };

            gMaster.assassinatedChar = selectTarget(invalidChars, Criteria.District);

            uiManager.sfxSource.PlayOneShot(uiManager.assassinKill);
            uiManager.log(playerName + " marked the " + gMaster.assassinatedChar + " for assassination");
        }

        new void steal() {
            Character.Name[] invalidChars = {
            Character.Name.None, Character.Name.Assassin, gMaster.assassinatedChar, characterFirst, characterSecond };

            gMaster.charToSteal = selectTarget(invalidChars, Criteria.Gold);

            uiManager.sfxSource.PlayOneShot(uiManager.thiefSteal);
            uiManager.log(playerName + " marked the " + gMaster.charToSteal + " for theft");
        }

        PlayerController_Single playerToExchange;
        List<int> lowScoreCards;
        bool decideSelectMagician() {
            playerToExchange = null;
            PlayerController_Single[] players = sortTopTargets(Criteria.Hand);
            int difference = players[players.Length - 1].hand.Count - hand.Count;

            //Select Magician if districts at hand is few and an opponent has more
            if(hand.Count <= CData.HAND_START_COUNT && difference >= CData.HAND_START_COUNT) {
                playerToExchange = players[players.Length - 1];
                return true;
            }

            //Calculate if districts at hand is bad, switch the low scores with deck
            lowScoreCards = new List<int>();
            prefDistCost = 4;
            float[] scores = getScores(hand.ToArray());
            for(int i = 0; i < scores.Length; i++) {
                if(scores[i] < distCostPrio)
                    lowScoreCards.Add(i);
            }
            //Select Magician if there's any card with a low score
            if(lowScoreCards.Count > 0 && gMaster.districtDeck.Count > 0)
                return true;

            //Don't select Magician otherwise
            return false;
        }

        //Perform planned move when calculating to select magician
        void decideExchange() {
            if(playerToExchange != null)
                exchangePlayer(playerToExchange.playerListI);
            else if(lowScoreCards != null && lowScoreCards.Count > 0)
                exchangeDeck(lowScoreCards);
        }

        new void exchangePlayer(int opponentI) {
            PlayerController_Single opponent = gMaster.playerList[opponentI];
            var temp = hand;
            hand = opponent.hand;
            opponent.hand = temp;
            uiManager.updateOpponent(opponentI);
            uiManager.updateOpponent(playerListI);
            uiManager.sfxSource.PlayOneShot(uiManager.magicianExchange);
            uiManager.log(playerName + " exchanged districts with " + opponent.playerName);
        }


        new void taxDistricts(District.Type taxedType) {
            int taxAmount = 0;
            foreach(District.Name district in builtDistricts) {
                if(CData.getCardType(district) == taxedType)
                    taxAmount++;
            }

            if(builtDistricts.Contains(District.Name.SchoolOfMagic)) {
                uiManager.log(playerName + " used " + District.Name.SchoolOfMagic + ", gained additional gold tax");
                taxAmount++;
            }

            if(taxAmount > 0) {
                gold += taxAmount;
                goldGainRound += taxAmount;

                uiManager.sfxSource.PlayOneShot(uiManager.opponentGold);
                uiManager.updateOpponent(playerListI);
                uiManager.log(playerName + " taxed " + taxedType + " districts, gained " + taxAmount + " gold");
            }
        }

        void destroyDistrict() {
            PlayerController_Single[] players = sortTopTargets(Criteria.District);

            for(int i = players.Length - 1; i >= 1; i--) {
                if((i / (float)players.Length) + distCostPrio > Random.value) {
                    //Skip player if Bishop is in possession
                    if(players[i] == gMaster.playerCharTurns[(int)Character.Name.Bishop]
                        || players[i].builtDistricts.Count >= CData.BUILD_COUNT_LIMIT)
                        continue;

                    //Iterate through player's districts
                    for(int distI = 0; distI < players[i].builtDistricts.Count; distI++) {
                        //Keep cannot be destroyed
                        if(players[i].builtDistricts[distI] == District.Name.Keep)
                            continue;

                        int targetDistCost = CData.getDistrictData(District.Data.Cost, players[i].builtDistricts[distI]) - 1;

                        //Negate cost deduction if opponent has Great Wall
                        if(players[i].builtDistricts.Contains(District.Name.GreatWall) &&
                            players[i].builtDistricts[distI] != District.Name.GreatWall) {
                            targetDistCost++;
                        }

                        if(targetDistCost <= gold) {
                            gMaster.destroyDistrict(players[i].playerListI, distI, targetDistCost);
                            return;
                        }
                    }
                }
            }
        }

        public override void graveyardChoice(District.Name district) {
            if(CData.getDistrictData(District.Data.Cost, district) > 2) {
                buyDestroyedDistrict();
            }
        }
    }
}