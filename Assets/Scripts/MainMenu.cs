using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;
using Prototype.NetworkLobby;
using System;

namespace Citadel
{
    public class MainMenu : MonoBehaviour
    {
        public static MainMenu instance;

        [Header("Background")]
        public RectTransform bg;
        public float bgScrollDur;
        public float bgEndYPos;
        public float bgScrollSpeed;
        public Image startGamePnl;
        public GameObject lobbyManagerGO;

        [Header("Start Button")]
        public Button startBtn;
        public Button loadBtn;
        public Color orgColor;
        public Color pressedColor;
        public float clrDuration;
        public float fadeSpeed;

        [Header("AI Settings")]
        public GameObject aiSettingsPnl;
        public Text oppCountTxt;
        public Slider oppCountSlider;
        public Text oppSpeedTxt;
        public Slider oppSpeedSlider;

        [Header("Instructions")]
        public ScrollRect instructionView;
        public RectTransform instructionText;
        public Image instructionImg;
        public Sprite[] instructions;
        public int instructionI;
        bool isShowRules;

        [Header("Player Info")]
        public InputField playerName;
        public GameObject highScorePnl;
        public Text highScoreTxt;
        public Image scoreCard;
        public Sprite[] scoreCards;
        public Color nameClr;

        [Header("Load Game Menu")]
        public GameObject saveEntryPrefab;
        public GameObject savedGamePnl;
        public RectTransform savedGameList;
        public Color singlePlayerClr;
        public Color multiPlayerClr;
        public int singleLineHeight;

        [Header("Volume")]
        public Slider bgmVolSlider;
        public Slider sfxVolSlider;

        GameMasterData[] savedGameData;
        string[] savedGameFiles;

        private void Awake() {
            instance = this;
        }

        void Start() {
            StartCoroutine(cScrollBG());

            //Control button function dynamically
            startBtn.onClick.AddListener(onPressSinglePlayer);

            //Check if player profile already exists, load if it does or create a new one if not
            if(CData.isProfileSaveExists()) {
                CData.loadProfile();
                playerName.text = CData.playerProfile.playerName;
            } else {
                new PlayerProfile();
                CData.saveProfile();
                StartCoroutine(cFlashNamePnl());
            }

#if UNITY_EDITOR
            CData.playerProfile.isTutorial = false;
#endif
            //Don't show highscore panel if no score has been recorded yet
            if(CData.playerProfile.highScore <= 0) {
                highScorePnl.SetActive(false);
            } else {
                highScorePnl.SetActive(true);
                highScoreTxt.text = CData.playerProfile.highScore.ToString();
                scoreCard.sprite = getScoreCard();
            }

            bgmVolSlider.value = CData.playerProfile.bgmVol;
            sfxVolSlider.value = CData.playerProfile.sfxVol;

            //Set load flag off by default
            CData.isLoadGame = false;

            refreshSavedGameList();

            //Reset static variables from previous game session
            PlayerController.playerCount = 0;
            GameMaster.instance = null;
            UIManager.instance = null;
        }

        void refreshSavedGameList() {
            //Retrieve saved game files if there's any
            savedGameFiles = CData.getSavedGames();

            //If save is present, create an entry for it at load game list/menu
            if(savedGameFiles.Length > 0) {
                //Arrange save files so single player game save is the first
                Array.Sort(savedGameFiles, new FileTypeComparer());

                loadBtn.gameObject.SetActive(true);
                populateSavedGameList();
            } else {
                loadBtn.gameObject.SetActive(false);
            }

            //Hide the list of saved games
            savedGamePnl.SetActive(false);
        }

        IEnumerator cScrollBG() {
            while(bgScrollDur > 0) {
                yield return new WaitForEndOfFrame();
                bg.anchoredPosition = Vector2.up * Mathf.Lerp(bg.anchoredPosition.y, bgEndYPos, Time.deltaTime * bgScrollSpeed);
                bgScrollDur -= Time.deltaTime;
            }
        }

        IEnumerator cFlashNamePnl() {
            float duration = 0;
            while(duration <= 1) {
                yield return new WaitForEndOfFrame();
                playerName.image.color = Color.Lerp(Color.white, nameClr, duration);
                duration += Time.deltaTime;
            }
        }

        //Make the start button show AI settings before starting
        void onPressSinglePlayer() {
            closeAllMenu();

            aiSettingsPnl.SetActive(true);
            startBtn.onClick.RemoveAllListeners();
            startBtn.onClick.AddListener(startSinglePlayerGame);
            StartCoroutine(lerpColor(startBtn.GetComponent<Image>(), pressedColor));

            CData.saveProfile();
        }

        void startSinglePlayerGame() {
            //Check if player name exists before starting the game
            if(playerName.text.Length <= 0) {
                StartCoroutine(cFlashNamePnl());
            } else {
                //Set load game flag off
                CData.isLoadGame = false;

                //Set multiplayer flag off
                CData.isMultiplayer = false;

                CData.saveFilePath = CData.systemPersistentPath + CData.FILE_PATH_SINGLE;
                StartCoroutine(mainMenuTransition());
            }
        }

        //Switch to Single Player game scene
        IEnumerator mainMenuTransition() {
            CData.opponentCount = (int)oppCountSlider.value;
            CData.gameSpeed = (int)oppSpeedSlider.value;
            CData.deleteGameSave(CData.FILE_PATH_SINGLE);

            float duration = 2;
            startGamePnl.CrossFadeAlpha(0, 0.001f, true);
            startGamePnl.gameObject.SetActive(true);
            startGamePnl.CrossFadeAlpha(2, duration, true);

            //Disable lobby manager used only for multiplayer
            //lobbyManagerGO.SetActive(false);

            savedGamePnl.SetActive(false);

            yield return new WaitForSeconds(duration);
            SceneManager.LoadScene(CData.SCENE_GAME_SINGLE);
        }

        public void onPressLoadBtn() {
            closeAllMenu();

            savedGamePnl.SetActive(true);

            CData.saveProfile();
        }

        public void onOppCountChange() {
            oppCountTxt.text = oppCountSlider.value.ToString();
        }

        public void onOppSpeedChange() {
            oppSpeedTxt.text = Math.Round(oppSpeedSlider.value, 2).ToString();
        }

        IEnumerator lerpColor(Image UIImg, Color targetClr) {
            float fadeDur = clrDuration;

            while(fadeDur > 0) {
                UIImg.color = Color.Lerp(UIImg.color, targetClr, Time.deltaTime * fadeSpeed);
                yield return new WaitForEndOfFrame();
                fadeDur -= Time.deltaTime;
            }
        }

        public void onInstructionScroll(Vector2 value) {
            if(!isShowRules) return;

            if(instructionI < instructions.Length - 1 && value.y < 0 && Input.GetMouseButtonUp(0)) {
                instructionI++;
                instructionImg.sprite = instructions[instructionI];
                instructionView.verticalNormalizedPosition = 1;
            } else if(instructionI > 0 && value.y > 1 && Input.GetMouseButtonUp(0)) {
                instructionI--;
                instructionImg.sprite = instructions[instructionI];
                instructionView.verticalNormalizedPosition = 0;
            }
        }

        public void onPressShowRules() {
            showRules(!isShowRules);
        }

        void showRules(bool isShow) {
            isShowRules = isShow;
            instructionText.gameObject.SetActive(!isShow);
            instructionImg.gameObject.SetActive(isShow);

            if(isShow)
                instructionView.content = instructionImg.GetComponent<RectTransform>();
            else
                instructionView.content = instructionText;
        }

        public void onPressInstructions() {
            closeAllMenu();

            showRules(false);
            instructionI = 0;
            instructionImg.sprite = instructions[instructionI];

            instructionView.verticalNormalizedPosition = 1;
            instructionView.gameObject.SetActive(!instructionView.gameObject.activeInHierarchy);
            isShowRules = false;
        }

        Sprite getScoreCard() {
            int scoreCardI = CData.playerProfile.totalScore / CData.HIGH_SCORE_CARD_MOD;
            scoreCardI = Mathf.Clamp(scoreCardI, 0, scoreCards.Length - 1);
            return scoreCards[scoreCardI];
        }

        public void setPlayerName(string name) {
            CData.playerProfile.playerName = name;
            CData.saveProfile();
        }

        public void onPressMultiplayer() {
            closeAllMenu();

            //Set load game flag off
            CData.isLoadGame = false;

            //Set multiplayer flag on
            CData.isMultiplayer = true;

            CData.saveFilePath = CData.systemPersistentPath + CData.generateFileName();

            CData.saveProfile();

            LobbyManager.s_Singleton.showLobby(true);
        }

        public void closeAllMenu() {
            instructionView.gameObject.SetActive(false);

            aiSettingsPnl.SetActive(false);
            startBtn.onClick.RemoveAllListeners();
            startBtn.onClick.AddListener(onPressSinglePlayer);
            StartCoroutine(lerpColor(startBtn.GetComponent<Image>(), orgColor));

            savedGamePnl.SetActive(false);

            LobbyManager.s_Singleton.showLobby(false);
        }

        void populateSavedGameList() {
            //Clear all saved game entries 
            foreach(RectTransform item in savedGameList) {
                Destroy(item.gameObject);
            }

            if(savedGameFiles.Length <= 0)
                return;

            //Store loaded data
            savedGameData = new GameMasterData[savedGameFiles.Length];

            //Iterate save files
            for(int i = 0; i < savedGameFiles.Length; i++) {
                //Load data to display info and store it
                GameMasterData data = CData.loadGameMaster(savedGameFiles[i]);

                savedGameData[i] = data;

                string gameInfo = data.timeCreated;
                gameInfo += "\n" + "Round: " + data.roundCount;

                string playerInfo = "";
                for(int j = 0; j < data.playerList.Count; j++) {
                    playerInfo += data.playerList[j].playerName;
                    if(j < data.playerList.Count - 1)
                        playerInfo += "\n";
                }

                //Create save list entry per file retrieved and display save entry info
                GameObject saveEntry = Instantiate(saveEntryPrefab);
                saveEntry.transform.SetParent(savedGameList);

                GameObject gameInfoTxt = saveEntry.transform.GetChild(0).gameObject;
                gameInfoTxt.GetComponent<Text>().text = gameInfo;

                GameObject playerInfoTxt = saveEntry.transform.GetChild(1).gameObject;
                playerInfoTxt.GetComponent<Text>().text = playerInfo;

                //Resize button/image of entry to match its contents
                RectTransform entryRT = saveEntry.GetComponent<RectTransform>();
                float textHeight = singleLineHeight * (data.playerList.Count + 1);
                RectTransform scrollViewRect = savedGamePnl.transform.GetChild(0).GetComponent<RectTransform>();
                entryRT.sizeDelta = new Vector2(scrollViewRect.rect.width, textHeight);

                //Indicate if multiplayer or single player by setting the color
                saveEntry.GetComponent<Image>().color = (data.isMultiplayer) ? multiPlayerClr : singlePlayerClr;

                //Add a listener to load the save file on click
                int fileIndex = i;
                saveEntry.GetComponent<Button>().onClick.AddListener(() => loadSaveFile(fileIndex));

                //Get Delete Button and add functionality
                saveEntry.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => deleteGameSave(fileIndex));
            }
        }

        GameMasterData loadedData;
        void loadSaveFile(int fileIndex) {
            //Set load game flag on
            CData.isLoadGame = true;

            loadedData = savedGameData[fileIndex];


            //Set multiplayer flag based on loaded data
            CData.isMultiplayer = loadedData.isMultiplayer;
            CData.loadedGameMaster = loadedData;
            CData.saveFilePath = savedGameFiles[fileIndex];

            if(loadedData.isMultiplayer) {
                //Show multiplayer lobby and wait for players to connect
                LobbyManager.s_Singleton.showLobby(true);
                StartCoroutine(cMultiplayerLoadLobby());
            } else {
                //If single player, simply load the game scene
                StartCoroutine(mainMenuTransition());
            }
        }

        public void deleteGameSave(int index) {
            closeAllMenu();
            CData.deleteGameSave(savedGameFiles[index]);
            refreshSavedGameList();
        }

        //Wait for end of frame so UI is properly setup
        IEnumerator cMultiplayerLoadLobby() {
            yield return new WaitForEndOfFrame();

            LobbyManager.s_Singleton.matchmakerJoinGO.SetActive(false);
            LobbyManager.s_Singleton.directPlayJoinGO.SetActive(false);
        }

        public void playerListModified() {
            Debug.Log("--- " + System.Reflection.MethodBase.GetCurrentMethod().Name + " ---");
            Debug.Log("  " + "LIST MODIFIED! IS LOAD GAME: " + CData.isLoadGame);
            if(CData.isLoadGame) {
                disableHostJoin(!isPlayersValid());
            }
        }

        public void playerReadyCallback() {
            if(CData.isLoadGame) {
                disableHostJoin(!isPlayersValid());
            }
        }

        bool isPlayersValid() {
            bool isValid = true;

            LobbyPlayerList lobbyPlayerList = LobbyPlayerList._instance;

            if(loadedData.playerList.Count != lobbyPlayerList._players.Count) {
                isValid = false;
            }

            for(int i = 1; i < lobbyPlayerList._players.Count; i++) {
                LobbyPlayer player = lobbyPlayerList._players[i];

                //Check if a player is included in saved game 
                bool isPlayerJoined = false;
                foreach(var loadedDataPlayer in loadedData.playerList) {
                    isPlayerJoined |= player.playerDataKey == loadedDataPlayer.key;
                }

                //Check if any player other than the host is not ready
                if(!isPlayerJoined || !player.readyToBegin)
                    isValid = false;
            }

            return isValid;
        }

        //Prevent game from starting
        void disableHostJoin(bool isDisable) {
            if(LobbyPlayerList._instance._players.Count <= 0)
                return;

            LobbyPlayer hostPlayer = LobbyPlayerList._instance._players[0].GetComponent<LobbyPlayer>();
            Text joinBtnText = hostPlayer.readyButton.transform.GetChild(0).GetComponent<Text>();

            hostPlayer.readyButton.interactable = !isDisable;

            if(isDisable) {
                hostPlayer.ChangeReadyButtonColor(LobbyPlayer.NotReadyColor);
                joinBtnText.text = "...";
            } else {
                hostPlayer.ChangeReadyButtonColor(LobbyPlayer.JoinColor);
                joinBtnText.text = "Start Game";
            }
        }

        //Arrange save files so single player game save is the first
        class FileTypeComparer : IComparer<string>
        {
            public int Compare(string x, string y) {
                if(x.EndsWith(CData.FILE_EXTENSION_SINGLE)) {

                    Debug.Log("--- " + System.Reflection.MethodBase.GetCurrentMethod().Name + " ---");
                    Debug.Log("  " + "FILE_EXTENSION_SINGLE");
                    return -1;
                } else
                    return 1;
            }
        }

        public void onBgmVolChange() {
            CData.playerProfile.bgmVol = bgmVolSlider.value;
        }

        public void onSfxVolChange() {
            CData.playerProfile.sfxVol = sfxVolSlider.value;
        }
    }
}