using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Acts as Dealer/Server
namespace Citadel
{
    public class GameMaster_Single : MonoBehaviour
    {
        public static GameMaster_Single instance;
        public GameObject aiPrefab;
        public UIManager_Single uiManager;

        [Range(1, 6)]
        public int aiCount = 1;
        [Range(0, 5)]
        public float transitionPauseDur = 3;

        //List to hold reference to all players, order of character selection follows list
        public List<PlayerController_Single> playerList { get; private set; }
        //Used for defining player turn based on character selected
        public PlayerController_Single[] playerCharTurns { get; private set; }
        public int curCharacterTurn { get; private set; }
        //Stores loaded player data to be retrieved by players when they start
        public List<PlayerData> playerListData { get; private set; }

        //Index used with playerList to indicate crown holder
        public int crownHolderI;

        public List<Character.Name> characterDeck;
        public Queue<District.Name> districtDeck;

        public List<Character.Name> characterDiscard;

        public bool isLastRound;
        public PlayerController_Single firstCompleteDist;

        public Character.Name assassinatedChar;
        public Character.Name charToSteal;
        public District.Name destroyedDistrict;

        public int roundCount;

        public string savedActivityLog;

        void Awake() {
            instance = this;

            //Set variables that determine the game's end before load or new game
            isLastRound = false;
            firstCompleteDist = null;

            //Skip initialization and load data from last save instead
            if(CData.isLoadGame) {
                copyLoadedData();
                addAI(aiCount);
                return;
            }

            //Initialize the district once for the whole game
            initDistrictDeck();

            //Initialize list to allow players to register on Start()
            playerList = new List<PlayerController_Single>();
            //Add AI based on game setup
            aiCount = CData.opponentCount;
            addAI(aiCount);

            switch(CData.gameSpeed) {
                case 1:
                    transitionPauseDur = 2.5f;
                    break;
                case 2:
                    transitionPauseDur = 1.75f;
                    break;
                case 3:
                    transitionPauseDur = 1;
                    break;
                default:
                    transitionPauseDur = 1.5f;
                    break;
            }

            crownHolderI = (int)Character.Name.None;

            roundCount = 0;
        }

        void saveData() {
            savedActivityLog = uiManager.activityLog.ToString();
            CData.saveGameMaster();
        }

        void copyLoadedData() {
            GameMasterData loadedData = CData.loadedGameMaster;
            aiCount = loadedData.aiCount;

            transitionPauseDur = loadedData.transitionPauseDur;

            playerListData = loadedData.playerList;
            playerList = new List<PlayerController_Single>();

            crownHolderI = loadedData.crownHolderI;

            districtDeck = new Queue<District.Name>();
            foreach(var district in loadedData.districtDeck) {
                districtDeck.Enqueue(CData.districtNames[district]);
            }

            roundCount = loadedData.roundCount;

            uiManager.loadActivityLog(loadedData.savedActivityLog);
        }

        void Start() {
            StartCoroutine(LateStart(0.5f));
        }

        //Delayed start to ensure that all players are registered in playerList
        IEnumerator LateStart(float delay) {
            yield return new WaitForSeconds(delay);

            //Skip initialization if game is loaded
            if(!CData.isLoadGame) {
                uiManager.log("Begin! \n" + playerList.Count + " Player Rule \n");

                //Show tutorial for new players
                if(CData.playerProfile.isTutorial)
                    StartCoroutine(uiManager.cShowTutorial(CData.TUTORIAL_GOAL));

                yield return new WaitForSeconds(transitionPauseDur);

                //Shuffle initial player list
                playerList = shuffleList(playerList);
                for(int i = 0; i < playerList.Count; i++) {
                    playerList[i].playerListI = i;
                }

                //Set the Crown Holder randomly
                uiManager.log("Selecting the Crown holder..");
                yield return new WaitForSeconds(transitionPauseDur);
                crownHolderI = Random.Range(0, playerList.Count);
            }

            //Initialize opponent data display
            uiManager.setupOpponentPanels();
            uiManager.updateCrown();
            uiManager.updateBuiltDistricts();
            uiManager.builtDistrictPnl.gameObject.SetActive(true);
            yield return new WaitForSeconds(transitionPauseDur);

            //Start game loop by starting character selection
            StartCoroutine(cCharSelectPhase());
        }

        //When game is loaded, sort PlayerList based on their saved indices
        void sortPlayerList() {
            for(int i = 0; i < playerList.Count; i++) {
                switchPlayers(i, playerList[i].playerListI);
            }
        }
        void switchPlayers(int ai, int bi) {
            PlayerController_Single pa = playerList[ai];
            playerList[ai] = playerList[bi];
            playerList[bi] = pa;
        }

        //Create/populate Character deck, 1 card per character, then shuffle
        void initCharacterDeck() {
            characterDeck = new List<Character.Name>();
            characterDiscard = new List<Character.Name>();

            //Iterate all Characters and place in deck
            foreach(Character.Name character in CData.characters) {
                characterDeck.Add(character);
            }
            //Remove null value from Character enum
            characterDeck.Remove(Character.Name.None);
        }

        //Create/populate District deck then shuffle
        //Card count varies per District, specified in CData class
        void initDistrictDeck() {
            districtDeck = new Queue<District.Name>();

            //Iterate all Districts and place in deck
            foreach(District.Name district in System.Enum.GetValues(typeof(District.Name))) {
                //Get specified card count
                int cardCount = CData.getDistrictData(District.Data.Count, district);

                for(int i = 0; i < cardCount; i++)
                    districtDeck.Enqueue(district);
            }

            districtDeck = new Queue<District.Name>(shuffleList(districtDeck.ToList()));
        }

        //Overwrite current deck with a shuffled copy
        public List<T> shuffleList<T>(List<T> deck) {
            int n = deck.Count;

            while(n > 1) {
                n--;
                int k = Random.Range(0, n + 1);
                T value = deck[k];
                deck[k] = deck[n];
                deck[n] = value;
            }

            return deck;
        }

        //Add an AI Opponent
        void addAI(int count) {
            count = Mathf.Clamp(count, 1, 6);

            for(int i = 0; i < count; i++) {
                GameObject go = Instantiate(aiPrefab);
                AIController ai = go.GetComponent<AIController>();
                ai.savedDataKey = i;
            }
        }

        void clearCharacters() {
            for(int i = 0; i < playerList.Count; i++) {
                playerList[i].characterFirst = Character.Name.None;
                playerList[i].characterSecond = Character.Name.None;
                uiManager.updateOpponent(i);
            }
            uiManager.updatePlayerCharacter();
        }

        //Starts character selection for each player
        IEnumerator cCharSelectPhase() {
            roundCount++;

            //Allow to close and reopen character select window
            uiManager.charSelBtn.gameObject.SetActive(true);
            uiManager.charSelCloseBtn.onClick.AddListener(() => {
                uiManager.resumeCharSelBtn.SetActive(true);
            });
            uiManager.log("\nCharacter selection phase begins \n");

            yield return new WaitForSeconds(transitionPauseDur);

            //Hide turn indicator since no characters yet
            uiManager.displayTurnIndicator(false);

            //Refresh turn list
            playerCharTurns = new PlayerController_Single[CData.CHARACTER_COUNT];
            clearCharacters();

            initCharacterDeck();

            //First facedown card
            characterDeck = shuffleList(characterDeck);
            characterDiscard.Add(characterDeck[0]);
            characterDeck.RemoveAt(0);
            uiManager.log("A character is faced down");

            //Pause for transition
            yield return new WaitForSeconds(transitionPauseDur);

            int faceupCards = 0;
            if(playerList.Count == 4)
                faceupCards = 2;
            else if(playerList.Count == 5)
                faceupCards = 1;

            for(int i = 0; i < faceupCards; i++) {
                characterDiscard.Add(characterDeck[0]);
                uiManager.log("Faceup: " + characterDeck[0]);
                characterDeck.RemoveAt(0);
            }

            //Start character selection from the player with crown
            int playerIndex = crownHolderI;

            //Players select and pass character cards, different rules are based on player count
            for(int i = 0; i < CData.CHARACTER_SELECT_ROUNDS[playerList.Count]; i++) {
                uiManager.log(playerList[playerIndex].playerName + "'s turn to choose a character");
                yield return new WaitForSeconds(transitionPauseDur);

                //Wait for player to finish selecting
                yield return StartCoroutine(playerList[playerIndex].cCharSelectPhase());

                //Assign player's turn according to selected character
                playerCharTurns[(int)playerList[playerIndex].characterFirst] = playerList[playerIndex];
                //Check if player has chosen a second character
                if(playerList[playerIndex].characterSecond != Character.Name.None)
                    playerCharTurns[(int)playerList[playerIndex].characterSecond] = playerList[playerIndex];


                //Proceed to next player
                playerIndex++;
                playerIndex %= playerList.Count;
            }
            uiManager.log("\n" + "Character selection phase ends \n");
            yield return new WaitForSeconds(transitionPauseDur);

            //Remove capability to reopen character select panel
            uiManager.charSelCloseBtn.onClick.RemoveAllListeners();
            uiManager.resumeCharSelBtn.SetActive(false);
            uiManager.charSelBtn.gameObject.SetActive(false);

            StartCoroutine(cDistrictPhase());
        }

        //Reset round variables
        void initDistrictPhase() {
            curCharacterTurn = 0;

            assassinatedChar = Character.Name.None;
            charToSteal = Character.Name.None;

            //Set crown holder to whoever picked the King early so its not affected by assassination
            if(playerCharTurns[(int)Character.Name.King] != null) {
                PlayerController_Single crownHolder = playerCharTurns[(int)Character.Name.King];
                crownHolderI = crownHolder.playerListI;
                uiManager.addSummary(crownHolder.playerName + " possesses the Crown");
            }

            //Show turn indicator
            uiManager.displayTurnIndicator(true);
            uiManager.log("District building phase begins \n");
        }

        //Players follow the order of selected character, build districts and use abilities
        IEnumerator cDistrictPhase() {
            initDistrictPhase();

            //Show tutorial for new players
            if(CData.playerProfile.isTutorial)
                StartCoroutine(uiManager.cShowTutorial(CData.TUTORIAL_DIST_PHASE));

            //Iterate through player turns
            for(int i = 0; i < playerCharTurns.Length; i++) {
                curCharacterTurn = i;
                //Display that character is not played on current round
                uiManager.updateTurn(i);

                yield return new WaitForSeconds(transitionPauseDur);

                //Skip the assassinated character
                if(i == (int)assassinatedChar) {
                    uiManager.log("The " + assassinatedChar + " has been assassinated \n");
                    if(playerCharTurns[i] != null)
                        uiManager.addSummary(playerCharTurns[(int)Character.Name.Assassin].playerName +
                            " assassinated " + playerCharTurns[i].playerName + "'s " + assassinatedChar);
                    continue;
                }

                //Check if no player selected the character, else start the player's turn
                if(playerCharTurns[i] == null) {
                    uiManager.log("No one hired the " + CData.characters[i] + "\n");
                    yield return new WaitForSeconds(transitionPauseDur);
                } else {
                    uiManager.updateOpponent(playerCharTurns[i].playerListI);

                    //Start and wait for player's turn to finish
                    yield return StartCoroutine(playerCharTurns[i].cDistrictPhase());

                    //Check if anyone has complete districts built
                    if(firstCompleteDist == null && playerCharTurns[i].builtDistricts.Count >= CData.BUILD_COUNT_LIMIT) {
                        firstCompleteDist = playerCharTurns[i];
                        isLastRound = true;
                        uiManager.log("\n" + firstCompleteDist.playerName + " has completed a city! \n");
                    }
                    yield return new WaitForSeconds(transitionPauseDur);
                }
            }
            uiManager.log("District building phase ends");

            getRoundSummary();
            uiManager.displayRoundSummary();
            uiManager.updateCrown();

            yield return new WaitForSeconds(transitionPauseDur);

            //Show tutorial for new players
            if(CData.playerProfile.isTutorial)
                StartCoroutine(uiManager.cShowTutorial(CData.TUTORIAL_SCORE));

            if(isLastRound)
                gameOver();
            else {
                //Save game progress at the end of the round
                saveData();
                CData.playerProfile.isTutorial = false;
                CData.saveProfile();
                StartCoroutine(cCharSelectPhase());
            }
        }

        void getRoundSummary() {
            foreach(var player in playerList) {
                if(player.goldGainRound >= CData.HIGH_GOLD_GAIN)
                    uiManager.addSummary(player.playerName + " was able to procure " + player.goldGainRound + " gold");

                if(player.builtDistrictRound >= CData.ARCHITECT_BUILD_BONUS + CData.BUILD_COUNT_TURN)
                    uiManager.addSummary(player.playerName + " was able to build " + player.builtDistrictRound + " districts");
            }
        }

        //Draw district cards from deck
        public District.Name[] drawDistricts(int count) {
            //Limit draw count with the number of districts left in deck
            count = Mathf.Clamp(count, 0, districtDeck.Count);

            District.Name[] districts = new District.Name[count];

            for(int i = 0; i < count; i++) {
                districts[i] = districtDeck.Dequeue();
            }
            return districts;
        }

        public void returnDistrict(District.Name card) {
            districtDeck.Enqueue(card);
        }

        void gameOver() {
            uiManager.displayTurnIndicator(false);
            uiManager.log("City value tally:");

            int[] scores = new int[playerList.Count];
            string[] names = new string[playerList.Count];
            for(int i = 0; i < playerList.Count; i++) {
                int score = playerList[i].cityScore();

                scores[i] = score;
                names[i] = playerList[i].playerName;

                //Record score if main/local player
                if(playerList[i].isLocalPlayer) {
                    CData.playerProfile.totalScore += score;
                    if(CData.playerProfile.highScore < score) {
                        CData.playerProfile.highScore = score;
                    }
                }
            }

            //Display score tally ascendingly
            System.Array.Sort(scores, names);
            for(int i = 0; i < playerList.Count; i++) {
                uiManager.log(names[i] + " " + scores[i]);
            }
            uiManager.log(names[names.Length - 1] + " claims victory!");

            uiManager.mainMenuBtn.SetActive(true);
            CData.deleteGameSave(CData.saveFilePath);
            CData.saveProfile();
        }

        //Destroy district
        public void destroyDistrict(int playerI, int districtI, int cost) {
            //Remove destroyed district from player's built list
            PlayerController_Single player = playerList[playerI];
            //Mark the destroyed district
            destroyedDistrict = player.builtDistricts[districtI];
            player.builtDistricts.RemoveAt(districtI);

            //Deduct destroy cost to perpetrator
            playerCharTurns[curCharacterTurn].gold -= cost;

            //Update UI for new info
            uiManager.sfxSource.PlayOneShot(uiManager.warlordDestroy);
            uiManager.updatePlayerGold();
            uiManager.updateBuiltDistricts();
            uiManager.updateOpponent(playerI);

            string message = playerCharTurns[curCharacterTurn].playerName + " destroyed " +
                player.playerName + "'s " + destroyedDistrict + " for ";
            message += (cost > 0) ? cost.ToString() + " gold" : "free";
            uiManager.log(message);
            uiManager.addSummary(message);

            presentGraveyardChoice();
        }

        void presentGraveyardChoice() {
            //Check if a player has built the Graveyard, present choice to buy destroyed district
            foreach(var graveOwner in playerList) {
                if(graveOwner.builtDistricts.Contains(District.Name.Graveyard) &&
                    playerCharTurns[(int)Character.Name.Warlord] != graveOwner && graveOwner.gold > 0) {
                    //Wait for player input to take district
                    graveOwner.graveyardChoice(destroyedDistrict);
                }
            }
        }
    }
}