using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace Citadel
{
    public class PlayerController : NetworkBehaviour
    {
        protected GameMaster gMaster;
        protected UIManager uiManager;

        public static int playerCount; ///TEST///

        [SyncVar]
        public int dataKey;
        //Placement in GameMaster turn list
        [SyncVar]
        public int playerListI;
        [SyncVar(hook = "hShowPlayerName")]
        public string playerName;
        [SyncVar(hook = "hGoldModified")]
        public int gold;

        public SyncListInt hand = new SyncListInt();
        public SyncListInt builtDistricts = new SyncListInt();

        [SyncVar(hook = "hCharModified_1")]
        public int characterFirst;
        [SyncVar(hook = "hCharModified_2")]
        public int characterSecond;

        [SyncVar]
        public int goldGainRound;
        [SyncVar]
        public int builtDistrictRound;

        [SyncVar]
        public bool isWaitInput;
        [SyncVar]
        public int selectedCharacterI;

        //Number of district that can be built during turn
        [SyncVar(hook = "hBuildCountMod")]
        public int buildCount;

        [SyncVar(hook = "hAbilityUsedMod")]
        public bool isAbilityUsed;
        [SyncVar(hook = "hLaboratoryUsedMod")]
        public bool isLaboratoryUsed;
        [SyncVar(hook = "hSmithyUsedMod")]
        public bool isSmithyUsed;

        bool isPlayerActionTurn;

        PlayerData loadedData;

        public override void OnStartClient() {
            //Increase player count after being loaded so game master can check if players are complete
            playerCount++;
        }

        public IEnumerator cGMasterStart() {
            gMaster = GameMaster.instance;
            uiManager = UIManager.instance;

            Debug.Log("  " + playerName + " added to Player List, count: " + gMaster.playerList.Count);

            builtDistricts.Callback = hDistBuiltMod;
            hand.Callback = hDistHandMod;

            name = playerName;

            if(isLocalPlayer) {
                uiManager.playerCon = this;
                CmdSyncDataKey(CData.playerProfile.CITADELS_DATA_KEY);

                CData.playerProfile.playerName = playerName;
                uiManager.setPlayerName(playerName);
                uiManager.updateBuiltDistricts();
                uiManager.builtDistrictPnl.gameObject.SetActive(true);
            }

            yield return null;
        }

        //Retrieve saved data from Game Master
        public void loadData() {
            if(!isServer)
                return;

            characterFirst = (int)Character.Name.None;
            characterSecond = (int)Character.Name.None;

            PlayerData data = null;
            foreach(var item in gMaster.playerListData) {
                if(item.key == dataKey)
                    data = item;
            }

            gold = data.gold;

            hand.Clear();
            foreach(var item in data.hand) {
                hand.Add(item);
            }

            builtDistricts.Clear();
            foreach(var item in data.builtDistricts) {
                builtDistricts.Add(item);
            }
        }

        //Initialize hand and give starting district cards from district deck
        public void initPlayer() {
            if(!isServer)
                return;

            characterFirst = (int)Character.Name.None;
            characterSecond = (int)Character.Name.None;

            gold = CData.GOLD_START_COUNT;

            hand.Clear();
            District.Name[] drawnCards = gMaster.drawDistricts(CData.HAND_START_COUNT);
            foreach(var card in drawnCards) {
                hand.Add((int)card);
            }

            builtDistricts.Clear();
        }

        public virtual IEnumerator cCharSelectPhase() {
            //Ensure that only the instance in the server gets to modify vars, clients just sync
            if(!isServer)
                yield break;

            goldGainRound = 0;
            builtDistrictRound = 0;

            //Seven Player Rule, the last player can select between last character and first discarded character
            if(gMaster.characterDeck.Count == 1 && gMaster.playerList.Count == 7) {
                gMaster.characterDeck.Add(gMaster.characterDiscard[0]);
                gMaster.characterDiscard.RemoveAt(0);
            }

            //Activate Character Select Panel
            RpcShowCharSelection("Select a character");

            //Show tutorial for new players
            gMaster.RpcShowTutorial(CData.TUTORIAL_CHAR_SEL);

            //Suspend execution while player is selecting
            yield return StartCoroutine(cWaitForInput());

            //Retrieve player selection and set it
            if(characterFirst == (int)Character.Name.None) {
                characterFirst = selectedCharacterI;
                gMaster.characterDeck.Remove(characterFirst);
            }
            //Two and Three-player Rule, all players use 2 Characters
            else if(gMaster.playerList.Count <= 3 && characterFirst != (int)Character.Name.None) {
                characterSecond = selectedCharacterI;
                gMaster.characterDeck.Remove(characterSecond);
            }

            //Two-Player Rule, need to discard when selecting a character
            if(gMaster.playerList.Count == 2) {
                //The first player to select character does not discard
                if(gMaster.characterDeck.Count < CData.CHARACTER_COUNT - 2) {
                    //Choose another time for discard
                    RpcShowCharSelection("Discard a character");

                    yield return StartCoroutine(cWaitForInput());

                    gMaster.characterDiscard.Add(selectedCharacterI);
                    gMaster.characterDeck.Remove(selectedCharacterI);
                }
            }
        }

        public virtual IEnumerator cDistrictPhase() {
            //Ensure that only the instance in the server gets to modify vars, clients just sync
            if(!isServer)
                yield break;

            onStartTurn();

            //Select Action
            RpcShowActionSelection();

            //Show tutorial for new players
            gMaster.RpcShowTutorial(CData.TUTORIAL_PLAYER_TURN);

            yield return StartCoroutine(cWaitForInput());

            //Allow player's other actions
            RpcShowActionBtns(true);

            yield return StartCoroutine(cWaitForInput());

            yield return StartCoroutine(cOnEndTurn());
        }

        //Apply effects on start of player's turn
        protected void onStartTurn() {
            isAbilityUsed = false;
            isSmithyUsed = false;
            isLaboratoryUsed = false;

            gMaster.RpcBroadcastLog(playerName + " hired the " + CData.characters[gMaster.curCharacterTurn]);

            //Check if character is marked for steal
            if(gMaster.charToSteal == gMaster.curCharacterTurn) {
                string message = gMaster.playerCharTurns[(int)Character.Name.Thief].playerName +
                                             " stole " + gold + " gold from " + playerName;
                gMaster.RpcBroadcastLog(message);

                if(gold >= CData.GOLD_START_COUNT)
                    gMaster.summaryLog.AppendLine(message);

                //Transfer gold from victim to player who used thief
                gMaster.playerCharTurns[(int)Character.Name.Thief].gold += gold;
                gMaster.playerCharTurns[(int)Character.Name.Thief].goldGainRound += gold;
                gold -= gold;

                RpcPlayFXSteal();
                RpcUpdateOpponent(playerListI);
            }

            //If player's character is the merchant, receive a gold
            if((int)Character.Name.Merchant == gMaster.curCharacterTurn) {
                gold++;
                goldGainRound++;

                RpcPlayFXMerch();
                RpcUpdateOpponent(playerListI);
                gMaster.RpcBroadcastLog(playerName + " receives one gold, a penny for your thoughts");
            }

            //Reset player's number of allowable districts to build
            buildCount = CData.BUILD_COUNT_TURN;

            //If player's character is the architect, receive bonus districts and can build multiple times
            if((int)Character.Name.Architect == gMaster.curCharacterTurn) {
                if(gMaster.districtDeck.Count > 0) {
                    District.Name[] drawnCards = gMaster.drawDistricts(CData.ARCHITECT_DRAW_BONUS);
                    foreach(var card in drawnCards) {
                        hand.Add((int)card);
                    }
                }

                buildCount += CData.ARCHITECT_BUILD_BONUS;
                RpcPlayFXArch();
                gMaster.RpcBroadcastLog(playerName + " receives " + CData.ARCHITECT_DRAW_BONUS + " districts, can build multiple times");
            }

            if((int)Character.Name.Bishop == gMaster.curCharacterTurn) {
                RpcPlayFXBish();
            }
        }

        [Command]
        public void CmdAddGold(int amount) {
            gold += amount;
            if(amount > 0)
                goldGainRound += amount;
        }

        [Command]
        public void CmdAddDistrict(int district) {
            hand.Add(district);
        }

        [Command]
        public void CmdBuildDistrict(int district) {
            builtDistricts.Add(district);
            hand.Remove(district);

            builtDistrictRound++;
            District.Name builtDistrict = CData.districtNames[district];

            gMaster.RpcBroadcastLog(playerName + " built " + builtDistrict);
            if(CData.getCardType(builtDistrict) == District.Type.Special)
                gMaster.summaryLog.AppendLine(playerName + " built a special district, the " + builtDistrict);
        }

        [ClientRpc]
        void RpcShowDestroy() {
            if(isLocalPlayer)
                uiManager.showDestroyPnl();
        }

        IEnumerator cOnEndTurn() {
            //Ensure that only the instance in the server gets to modify vars, clients just sync
            if(!isServer)
                yield break;

            //Show Warlord ability UI
            if((int)Character.Name.Warlord == gMaster.curCharacterTurn) {
                RpcShowDestroy();
                yield return StartCoroutine(cWaitForInput());
            }

            gMaster.RpcBroadcastLog(playerName + " End Turn \n");
            RpcShowActionBtns(false);

            //Ensures that player will not be able to take action outside their turn
            buildCount = 0;
            isAbilityUsed = true;
            isSmithyUsed = true;
            isLaboratoryUsed = true;
        }

        //Compute player score based on districts built
        public int cityScore() {
            int score = 0;

            //Sum of district costs
            foreach(int district in builtDistricts) {
                score += CData.getDistrictData(District.Data.Cost, CData.districtNames[district]);
            }

            //Bonus for having complete districts
            if(builtDistricts.Count >= CData.BUILD_COUNT_LIMIT)
                score += CData.BONUS_COMPLETION;

            //Bonus for being the first one to complete
            if(gMaster.firstPlayerIndex == playerListI)
                score += CData.BONUS_FIRST;

            //Bonus for building a district for each type
            bool[] builtTypes = new bool[CData.districtTypes.Length];
            for(int i = 0; i < builtDistricts.Count; i++) {
                builtTypes[getTypeIndex(CData.districtNames[builtDistricts[i]])] = true;
            }

            int typeBuiltCtr = 0;
            foreach(bool builtType in builtTypes) {
                if(builtType)
                    typeBuiltCtr++;
            }

            if(builtDistricts.Contains((int)District.Name.HauntedCity))
                typeBuiltCtr++;

            if(typeBuiltCtr >= builtTypes.Length)
                score += CData.BONUS_ALL_TYPES;

            if(builtDistricts.Contains((int)District.Name.DragonGate))
                score += CData.BONUS_DRAGON_GATE;

            if(builtDistricts.Contains((int)District.Name.University))
                score += CData.BONUS_UNIVERSITY;

            return score;
        }

        [ClientRpc]
        public void RpcRecordScore(int score) {
            if(!isLocalPlayer)
                return;

            CData.playerProfile.totalScore += score;
            if(CData.playerProfile.highScore < score) {
                CData.playerProfile.highScore = score;
            }

            uiManager.mainMenuBtn.SetActive(true);
            CData.saveProfile();
        }

        protected int getTypeIndex(District.Name card) {
            for(int j = 0; j < CData.districtTypes.Length; j++) {
                if((int)card <= (int)CData.districtTypes[j])
                    return j;
            }
            return -1;
        }

        //Cause a character to miss a turn
        [Command]
        public void CmdAssassinate() {
            gMaster.assassinatedChar = selectedCharacterI;

            string charName = CData.characters[selectedCharacterI].ToString();
            gMaster.RpcBroadcastLog(playerName + " marked the " + charName + " for assassination");
            RpcPlayFXAssassinate();

            isAbilityUsed = true;
        }

        //Choose a character, steal all their turn on start of their turn
        [Command]
        public void CmdSteal() {
            gMaster.charToSteal = selectedCharacterI;

            string charName = CData.characters[selectedCharacterI].ToString();
            gMaster.RpcBroadcastLog(playerName + " marked the " + charName + " for theft");
            RpcPlayFXTarget();

            isAbilityUsed = true;
        }

        //Choose an opponent, swap their hand with yours
        [Command]
        public void CmdExchangePlayer(int opponentI) {
            PlayerController opponent = gMaster.playerList[opponentI];
            int[] tempHand = new int[hand.Count];
            hand.CopyTo(tempHand, 0);

            hand.Clear();
            foreach(var district in opponent.hand) {
                hand.Add(district);
            }

            opponent.hand.Clear();
            foreach(var district in tempHand) {
                opponent.hand.Add(district);
            }

            string message = playerName + " exchanged districts with " + opponent.playerName;
            gMaster.RpcBroadcastLog(message);
            gMaster.summaryLog.AppendLine(message);
            RpcPlayFXExchange();

            isAbilityUsed = true;
        }

        //Select cards from hand, return to district deck and get same amount in
        [Command]
        public void CmdExchangeDeck(int[] exchangeList) {
            //Return selected cards to deck
            int[] districts = new int[exchangeList.Length];
            for(int i = 0; i < exchangeList.Length; i++) {
                districts[i] = hand[exchangeList[i]];
                gMaster.districtDeck.Add(hand[exchangeList[i]]);
            }
            //Remove selected cards from hand in a separate loop to avoid messing indices
            for(int i = 0; i < districts.Length; i++) {
                hand.Remove(districts[i]);
            }

            //Draw same amount that was returned
            District.Name[] draws = gMaster.drawDistricts(exchangeList.Length);
            foreach(var card in draws) {
                hand.Add((int)card);
            }

            gMaster.RpcBroadcastLog(playerName + " exchanged " + exchangeList.Length + " districts back to deck");
            RpcPlayFXExchange();

            isAbilityUsed = true;
        }

        public int getTaxAmount(District.Type taxedType) {
            int taxAmount = 0;

            //Iterate through built districts and get gold for matching type
            foreach(int district in builtDistricts) {
                if(CData.getCardType(CData.districtNames[district]) == taxedType)
                    taxAmount++;
            }

            if(builtDistricts.Contains((int)District.Name.SchoolOfMagic))
                taxAmount++;

            return taxAmount;
        }

        //Get the district type affiliated with current character, get a gold for each district built of said type
        [Command]
        public void CmdTaxDistricts(int typeInt, int taxAmount) {
            if(isAbilityUsed)
                return;

            District.Type taxedType = District.Type.Military;

            for(int i = 0; i < CData.districtTypes.Length; i++) {
                if((int)CData.districtTypes[i] == typeInt)
                    taxedType = CData.districtTypes[i];
            }

            string message = playerName + " taxed " + taxedType + " districts, ";
            if(taxAmount > 0) {
                gold += taxAmount;
                goldGainRound += taxAmount;

                message += "gained " + taxAmount + " gold";
            } else {
                message += "but doesn't have such districts :(";
            }
            gMaster.RpcBroadcastLog(message);

            if(builtDistricts.Contains((int)District.Name.SchoolOfMagic))
                gMaster.RpcBroadcastLog(playerName + " used " + District.Name.SchoolOfMagic + ", gained additional gold tax");

            isAbilityUsed = true;
        }

        [Command]
        public void CmdDestroyDistrict(int playerI, int districtI, int cost) {
            gMaster.destroyDistrict(playerI, districtI, cost);
            RpcUpdateOpponent(playerI);
            RpcPlayFXDestroy();
        }

        [ClientRpc]
        public void RpcGraveyardChoice(int district) {
            if(!isLocalPlayer)
                return;

            uiManager.showChoice("Buy the destroyed district for one gold?", uiManager.DistrictCards[district]);
            uiManager.choiceYesBtn.onClick.RemoveAllListeners();
            uiManager.choiceYesBtn.onClick.AddListener(CmdBuyDestroyedDistrict);
        }

        //Salvage district
        [Command]
        public void CmdBuyDestroyedDistrict() {
            gold--;
            hand.Add(gMaster.destroyedDistrict);
            RpcPlayFXDist();
            RpcUpdateOpponent(playerListI);
            gMaster.RpcBroadcastLog(playerName + " used " + District.Name.Graveyard + ", salvaged " + gMaster.destroyedDistrict);
        }

        [ClientRpc]
        void RpcPlayFXDist() {
            uiManager.sfxSource.PlayOneShot(uiManager.districtCoins);
        }

        //Pay gold to gain districts
        [Command]
        public void CmdSmithy() {
            District.Name[] drawnCards = gMaster.drawDistricts(CData.SMITHY_DRAW);
            foreach(var card in drawnCards) {
                hand.Add((int)card);
            }
            gold -= CData.SMITHY_COST;
            gMaster.RpcBroadcastLog(playerName + " used " + District.Name.Smithy + ", obtained " + CData.SMITHY_DRAW + " districts");
        }

        //Discard a district to gain gold
        [Command]
        public void CmdLaboratory(int district) {
            gMaster.districtDeck.Add(district);
            hand.Remove(district);
            gold++;
            goldGainRound++;
            gMaster.RpcBroadcastLog(playerName + " used " + District.Name.Laboratory + ", traded a districts for a gold");
        }

        IEnumerator cWaitForInput() {
            isWaitInput = true;
            yield return new WaitWhile(() => isWaitInput);
        }

        //Set name in a method to be overridden easily
        [Command]
        protected void CmdSyncDataKey(int key) {
            dataKey = key;
        }

        [Command]
        public void CmdLog(string message) {
            gMaster.RpcBroadcastLog(message);
        }


        [Command]
        public void CmdAddSummary(string message) {
            gMaster.summaryLog.AppendLine(message);
        }

        [Command]
        public void CmdSetSelectedChar(int index) {
            selectedCharacterI = index;
        }

        [Command]
        public void CmdWaitEnd() {
            isWaitInput = false;
        }

        [Command]
        public void CmdReturnDistDeck(int card) {
            gMaster.districtDeck.Add(card);
        }

        [Command]
        public void CmdSetupOpponentPanelsCallback() {
            gMaster.playerListCompleted.Add(true);
        }

        [ClientRpc]
        void RpcShowCharSelection(string description) {
            if(isLocalPlayer) {
                uiManager.initCharSelect(description);
                int[] selection = new int[gMaster.characterDeck.Count];
                gMaster.characterDeck.CopyTo(selection, 0);
                uiManager.setupChar(selection, true);
            }
        }

        [ClientRpc]
        void RpcUpdateOpponent(int index) {
            uiManager.updateOpponent(index);
        }

        [ClientRpc]
        void RpcShowActionSelection() {
            if(isLocalPlayer)
                uiManager.showActionSelect();
        }

        [ClientRpc]
        void RpcShowActionBtns(bool isShow) {
            if(isLocalPlayer) {
                isPlayerActionTurn = isShow;
                uiManager.showActionBtns(isShow);
            }
        }

        [ClientRpc]
        void RpcPlayFXAssassinate() {
            uiManager.sfxSource.PlayOneShot(uiManager.assassinKill);
        }

        [ClientRpc]
        void RpcPlayFXTarget() {
            uiManager.sfxSource.PlayOneShot(uiManager.thiefTarget);
        }

        [ClientRpc]
        void RpcPlayFXSteal() {
            uiManager.sfxSource.PlayOneShot(uiManager.thiefSteal);
        }
        [ClientRpc]
        void RpcPlayFXExchange() {
            uiManager.sfxSource.PlayOneShot(uiManager.magicianExchange);
        }

        [ClientRpc]
        void RpcPlayFXBish() {
            uiManager.sfxSource.PlayOneShot(uiManager.bishopBell);
        }

        [ClientRpc]
        void RpcPlayFXMerch() {
            uiManager.sfxSource.PlayOneShot(uiManager.merchantCoin);
        }

        [ClientRpc]
        void RpcPlayFXArch() {
            uiManager.sfxSource.PlayOneShot(uiManager.architectScribble);
        }

        [ClientRpc]
        void RpcPlayFXDestroy() {
            uiManager.sfxSource.PlayOneShot(uiManager.warlordDestroy);
        }

        void hGoldModified(int newAmount) {
            gold = newAmount;
            uiManager.updateOpponent(playerListI);
            uiManager.sfxSource.PlayOneShot(uiManager.goldGain);
            if(isLocalPlayer) {
                uiManager.updatePlayerGold();
            }
        }

        void hCharModified_1(int newChar) {
            characterFirst = newChar;
            updateCharacter();
        }
        void hCharModified_2(int newChar) {
            characterSecond = newChar;
            updateCharacter();
        }
        void updateCharacter() {
            uiManager.updateOpponent(playerListI);
            if(isLocalPlayer)
                uiManager.updatePlayerCharacter();
        }

        void hBuildCountMod(int newVal) {
            buildCount = newVal;
            updateActionBtns();
        }
        void hAbilityUsedMod(bool newVal) {
            isAbilityUsed = newVal;
            updateActionBtns();
        }
        void hLaboratoryUsedMod(bool newVal) {
            isLaboratoryUsed = newVal;
            updateActionBtns();
        }
        void hSmithyUsedMod(bool newVal) {
            isSmithyUsed = newVal;
            updateActionBtns();
        }
        void updateActionBtns() {
            if(gMaster.curCharacterTurn == characterFirst
                || gMaster.curCharacterTurn == characterSecond) {
                uiManager.showActionBtns(isPlayerActionTurn);
            }
        }

        void hDistHandMod(SyncListInt.Operation op, int itemIndex) {
            uiManager.updateOpponent(playerListI);
        }

        void hDistBuiltMod(SyncListInt.Operation op, int itemIndex) {
            uiManager.updateOpponent(playerListI);

            if(op == SyncList<int>.Operation.OP_ADD)
                uiManager.sfxSource.PlayOneShot(uiManager.buildSfx);

            if(isLocalPlayer) {
                uiManager.updateBuiltDistricts();
            }
        }

        void hShowPlayerName(string newName) {
            playerName = newName;
            name = newName;
            if(isLocalPlayer) {
                CData.playerProfile.playerName = newName;
            }
        }
    }
}